/**
 * Created by sujata on 10/20/2016.
 */

var minerva = require('../../pages/minerva'),
    testData = require(minerva.getTestDataFilePath());

describe('results table', function() {
    beforeEach(function () {
        minerva.login(browser.params.userExternal);
        minerva.navbar.tabResults.then(function (recordsTab) {
            recordsTab.visit();
            columns = minerva.results.resultsTable.columns;
        });
    });

    it('Search order id not linked to physician ID: MI-3336', function () {
        minerva.results.btnDropdown.click();
        minerva.results.advanceSearch.InputOrderIdAdvance.sendKeys(testData.nonExistingData)
        minerva.results.advanceSearch.btnAdvanceSearch.click()
        expect(minerva.results.txtKeyword.getText()).toEqual('Order ID: '+testData.nonExistingData)
        expect(minerva.results.btnClear.isDisplayed()).toBeTruthy();
        expect(minerva.results.advanceSearch.noResultFound.getText()).toEqual(testData.noResultsText)
    });

    afterEach(function () {
        minerva.logout();
    });
});
