/**
 * Created by sujata on 10/19/2016.
 */
var minerva = require('../../pages/minerva'),
    moment = require('moment');

describe('results table', function() {
    beforeEach(function () {
        minerva.login(browser.params.userInternal);
        minerva.navbar.tabResults.then(function (recordsTab) {
            recordsTab.visit();
            columns = minerva.results.resultsTable.columns;
        });
    });

    it('Verify today button action on calender ID: MI-3313', function () {
        minerva.results.btnDropdown.click();
        minerva.results.advanceSearch.InputOrderDateFrom.click();
        expect(minerva.results.calender.tblCalander.isDisplayed()).toBeTruthy();
        expect(minerva.results.calender.btnToday.isDisplayed()).toBeTruthy();
        minerva.results.calender.btnToday.click();
        expect(minerva.results.advanceSearch.InputOrderDateFrom.getAttribute('value')).toEqual(moment().format("DD MMM YYYY"))
    });

    afterEach(function () {
        minerva.logout();
    });
});
