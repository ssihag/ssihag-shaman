/**
 * Created by sujata on 10/21/2016.
 */

var minerva = require('../../pages/minerva');


describe('results table', function() {
    beforeEach(function () {
        minerva.login(browser.params.userInternal);
        minerva.navbar.tabResults.then(function (recordsTab) {
            recordsTab.visit();
            columns = minerva.results.resultsTable.columns;
        });
    });

    it('Verify date in case of leap year ID: MI-3316', function () {
        minerva.results.btnDropdown.click();
        minerva.results.advanceSearch.InputOrderDateFrom.click();
        minerva.results.calender.btnForward.click();
        var nextMonthDate=moment().add(1,'months');
        expect(minerva.results.calender.txtMonth.getText()).toEqual(moment(nextMonthDate).format("MMMM YYYY"));
        minerva.results.calender.btnBackward.click();
        expect(minerva.results.calender.txtMonth.getText()).toEqual(moment().format("MMMM YYYY"));
        var year = minerva.results.calender.txtMonth.getText();
        minerva.results.calender.txtMonth.click();
        expect(minerva.results.calender.lstMonthsText1).toEqual(testData.monthList)
        // expect(minerva.results.calender.txtMonth.getText()).toEqual(b)
    });

    afterEach(function () {
        minerva.logout();
    });
});

