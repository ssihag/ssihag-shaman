/**
 * Created by sujata on 10/21/2016.
 */

var minerva = require('../../pages/minerva');
testData = require(minerva.getTestDataFilePath());

describe('results table', function() {
    beforeEach(function () {
        minerva.login(browser.params.userInternal);
        minerva.navbar.tabResults.then(function (recordsTab) {
            recordsTab.visit();
            columns = minerva.results.resultsTable.columns;
        });
    });

    it('perform search for order date from ID: MI-3318', function () {
        minerva.results.btnDropdown.click();
        minerva.results.advanceSearch.InputOrderDateFrom.sendKeys(testData.date);
        minerva.results.advanceSearch.btnAdvanceSearch.click()
        expect(minerva.results.txtKeyword.getText()).toEqual(testData.keyword+testData.date)
        expect(minerva.results.btnClear.isDisplayed()).toBeTruthy();
        expect(columns['PatientName'].data()).toEqual(testData.PatientName);
        expect(columns['ValueId'].data()).toEqual(testData.PatientId);
        expect(columns['ValueBorn'].data()).toEqual(testData.Dobs);
        expect(columns['ReportType'].data()).toEqual(testData.ReportType);
        expect(columns['ReportStatus'].data()).toEqual(testData.ReportStatus);
        expect(columns['ReportDescription'].data()).toEqual(testData.ReportDescription);
        expect(columns['ReportTime'].data()).toEqual(testData.ReportDate);
        expect(columns['ReportLocation'].data()).toEqual(testData.ReportLocation);
        expect(columns['ImageAvailableIcon'].data()).toEqual(testData.ImageIcon);
        expect(columns['ReportLabelIcon'].data()).toEqual(testData.ReportIcon);
        expect(columns['LabelId'].data()).toEqual(Array(testData.firstPageCount).fill(testData.IdText));
        expect(columns['LabelBorn'].data()).toEqual(Array(testData.firstPageCount).fill(testData.BornText));
        expect(minerva.results.getCurrentResultsRange()).toEqual('1 - '+testData.firstPageCount);
        expect(minerva.results.getTotalResultsCount()).toEqual(testData.totalCount);
    });

    afterEach(function () {
        minerva.logout();
    });
});


