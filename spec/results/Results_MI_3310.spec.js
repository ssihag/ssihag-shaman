/**
 * Created by sujata on 10/21/2016.
 */

var minerva = require('../../pages/minerva');
testData = require(minerva.getTestDataFilePath());

describe('results table', function() {
    beforeEach(function () {
        minerva.login(browser.params.userInternal);
        minerva.navbar.tabResults.then(function (recordsTab) {
            recordsTab.visit();
            columns = minerva.results.resultsTable.columns;
        });
    });

    it('Verify previous and next button actions ID: MI-3310', function () {
        minerva.results.btnDropdown.click();
        minerva.results.advanceSearch.InputOrderDateFrom.click();
        minerva.results.calender.btnForward.click();
        var nextMonthDate=moment().add(1,'months');
        expect(minerva.results.calender.txtMonth.getText()).toEqual(moment(nextMonthDate).format("MMMM YYYY"));
        minerva.results.calender.btnBackward.click();
        expect(minerva.results.calender.txtMonth.getText()).toEqual(moment().format("MMMM YYYY"));
        var d=new Date();
        var year=d.getFullYear().toString()
        minerva.results.calender.txtMonth.click();
        expect(minerva.results.calender.lstMonthsText1).toEqual(testData.monthList)
        expect(minerva.results.calender.txtMonth.getText()).toEqual(year)
        minerva.results.calender.btnForward.click();
        year++;
        expect(minerva.results.calender.txtMonth.getText()).toEqual(year.toString())
        minerva.results.calender.btnBackward.click();
        year--;
        expect(minerva.results.calender.txtMonth.getText()).toEqual(year.toString());
        minerva.results.calender.txtMonth.click();
        expect(minerva.results.calender.txtMonth.getText()).toEqual(testData.yearSpan);
        expect(minerva.results.calender.lstMonthsText1).toEqual(testData.yearList)
        minerva.results.calender.btnForward.click();
        expect(minerva.results.calender.lstMonthsText1).toEqual(testData.yearList1)
        expect(minerva.results.calender.txtMonth.getText()).toEqual(testData.yearSpan1);
        minerva.results.calender.btnBackward.click();
        expect(minerva.results.calender.lstMonthsText1).toEqual(testData.yearList)
        expect(minerva.results.calender.txtMonth.getText()).toEqual(testData.yearSpan);
    });

    afterEach(function () {
        minerva.logout();
    });
});

