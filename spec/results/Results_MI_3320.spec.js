/**
 * Created by sujata on 10/21/2016.
 */

var minerva = require('../../pages/minerva');
testData = require(minerva.getTestDataFilePath());

describe('results table', function() {
    beforeEach(function () {
        minerva.login(browser.params.userInternal);
        minerva.navbar.tabResults.then(function (recordsTab) {
            recordsTab.visit();
            columns = minerva.results.resultsTable.columns;
        });
    });

    it('Search for order date from greater than order date to ID: MI-3320', function () {
        minerva.results.btnDropdown.click();
        minerva.results.advanceSearch.InputOrderDateFrom.sendKeys(testData.toDate);
        minerva.results.advanceSearch.InputOrderDateTo.sendKeys(testData.fromDate);
       // minerva.results.advanceSearch.patientIdTextAdvance.click()
        expect(minerva.results.advanceSearch.txtDateError.isDisplayed()).toBeTruthy();
        expect(minerva.results.advanceSearch.txtDateError.getText()).toEqual(testData.msgError)
    });

    afterEach(function () {
        minerva.logout();
    });
});


