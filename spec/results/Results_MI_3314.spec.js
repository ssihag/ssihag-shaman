/**
 * Created by sujata on 10/19/2016.
 */

var minerva = require('../../pages/minerva');

describe('results table', function() {
    beforeEach(function () {
        minerva.login(browser.params.userInternal);
        minerva.navbar.tabResults.then(function (recordsTab) {
            recordsTab.visit();
            columns = minerva.results.resultsTable.columns;
        });
    });

    it('Verify clear button action on calender ID: MI-3314', function () {
        minerva.results.btnDropdown.click();
        minerva.results.advanceSearch.InputOrderDateFrom.click();
        expect(minerva.results.calender.tblCalander.isDisplayed()).toBeTruthy();
        expect(minerva.results.calender.btnClear.isDisplayed()).toBeTruthy();
        minerva.results.calender.btnForward.click();
        minerva.results.calender.selectItem('08');
        minerva.results.advanceSearch.InputOrderDateFrom.click();
        minerva.results.calender.btnClear.click();
        expect(minerva.results.advanceSearch.InputOrderDateFrom.getAttribute('value')).toEqual('')
        minerva.results.advanceSearch.InputOrderDateFrom.click();
        var nextMonthDate=moment().add(1,'months');
        expect(minerva.results.calender.txtMonth.getText()).toEqual(moment(nextMonthDate).format("MMMM YYYY"))
    });

    afterEach(function () {
        minerva.logout();
    });
});
