/**
 * Created by sujata on 10/20/2016.
 */
var minerva = require('../../pages/minerva'),
    moment = require('momentjs');

describe('results table', function() {
    beforeEach(function () {
        minerva.login(browser.params.userInternal);
        minerva.navbar.tabResults.then(function (recordsTab) {
            recordsTab.visit();
            columns = minerva.results.resultsTable.columns;
        });
    });

    it('Verify close button action on calender ID: MI-3315', function () {
        minerva.results.btnDropdown.click();
        minerva.results.advanceSearch.InputOrderDateFrom.click();
        expect(minerva.results.calender.tblCalander.isDisplayed()).toBeTruthy();
        expect(minerva.results.calender.btnClose.isDisplayed()).toBeTruthy();
        minerva.results.calender.btnClose.click();
        expect(minerva.results.calender.tblCalander.isDisplayed()).toBeFalsy();
    });

    afterEach(function () {
        minerva.logout();
    });
});
