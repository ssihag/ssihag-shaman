/**
 * Created by sujata on 10/26/2016.
 */
var minerva = require('../../pages/minerva'),
    testData = require(minerva.getTestDataFilePath());
var _ = require('lodash');

describe('Conditions table', function() {
    var columns;
    beforeAll(function () {
        minerva.login(browser.params.userInternal);
        minerva.patients.txtName.sendKeys(testData.patientName);
        minerva.patients.btnSearch.click();
        minerva.patients.patientsTable.row(1).then(function (rowOne) {
            rowOne.patientDetails().click();
        });
        minerva.navbar.innerTab('Conditions').then(function (conditionsTab) {
            conditionsTab.visit();
        });
    });

    it('MI-2464 verify spinner till loading of page', function () {
        expect(minerva.allergy.spinner.isDisplayed()).toBeTruthy();
        expect(minerva.allergy.allergyTable.title).toEqual('Conditions');
    });

    afterEach(function () {
        minerva.logout();
    });
});

