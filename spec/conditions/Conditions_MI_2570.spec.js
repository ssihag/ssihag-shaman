/**
 * Created by sujata on 10/27/2016.
 */

var minerva = require('../../pages/minerva'),
    testData = require(minerva.getTestDataFilePath());
var _ = require('lodash');

describe('Conditions table', function() {
    var columns;
    beforeAll(function () {
        minerva.login(browser.params.userInternal);
        minerva.patients.txtName.sendKeys(testData.patientName);
        minerva.patients.btnSearch.click();
        minerva.patients.patientsTable.row(1).then(function (rowOne) {
            rowOne.patientDetails().click();
        });
        minerva.navbar.innerTab('Conditions').then(function (conditionsTab) {
            conditionsTab.visit();
        });

    });

    it('MI-2570 back button functionality', function () {
        expect(minerva.conditions.conditionsTable.title).toEqual(testData.title);
        browser.navigate().back();
        minerva.navbar.innerTab('Conditions').then(function (conditionsTab) {
            expect(conditionsTab.displayed()).toBeTruthy();
        });
    });

    afterEach(function () {
        minerva.logout();
    });
});

