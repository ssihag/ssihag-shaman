/**
 * Created by sujata on 10/26/2016.
 */
var minerva = require('../../pages/minerva'),
    testData = require(minerva.getTestDataFilePath());

describe('Conditions table', function() {
    beforeAll(function () {
        minerva.login(browser.params.userInternal);
        minerva.patients.txtName.sendKeys(testData.patientName);
        minerva.patients.btnSearch.click();
        minerva.patients.patientsTable.row(1).then(function (rowOne) {
            rowOne.patientDetails().click();
        });
        minerva.navbar.innerTab('Conditions').then(function (conditionsTab) {
            conditionsTab.visit();
        });
    });

    it('MI-5493 active/all button should be present', function () {
        expect(minerva.conditions.conditionsTable.title).toEqual(testData.title)
        expect(minerva.conditions.btnActive.isDisplayed()).toBeTruthy()
        expect(minerva.conditions.btnAll.isDisplayed()).toBeTruthy()
    });

    afterEach(function () {
        minerva.logout();
    });
});