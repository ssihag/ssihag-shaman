/**
 * Created by sujata on 10/26/2016.
 */
var minerva = require('../../pages/minerva'),
    testData = require(minerva.getTestDataFilePath());

describe('Conditions table', function() {
    beforeAll(function () {
        minerva.login(browser.params.userInternal);
        minerva.patients.txtName.sendKeys(testData.patientName);
        minerva.patients.btnSearch.click();
        minerva.patients.patientsTable.row(1).then(function (rowOne) {
            rowOne.patientDetails().click();
        });
    });

    it('MI-2456 verify condition submenu item', function () {
        minerva.navbar.innerTab('Conditions').then(function (conditionsTab) {
            conditionsTab.visit();
        });
        expect(minerva.conditions.conditionsTable.title).toEqual(testData.title)
    });

    afterEach(function () {
        minerva.logout();
    });
});