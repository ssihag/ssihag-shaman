/**
 * Created by sujata on 10/27/2016.
 */

var minerva = require('../../pages/minerva'),
    testData = require(minerva.getTestDataFilePath());
var _ = require('lodash');

describe('conditions table', function() {
    var columns;
    beforeAll(function () {
        minerva.login(browser.params.userInternal);
        minerva.patients.txtName.sendKeys(testData.patientName);
        minerva.patients.btnSearch.click();
        minerva.patients.patientsTable.row(1).then(function (rowOne) {
            rowOne.patientDetails().click();
        });
        minerva.navbar.innerTab('Conditions').then(function (conditionsTab) {
            conditionsTab.visit();
        });
        columns = minerva.conditions.conditionsTable.columns;
    });

    it('MI-2461 verify values under corresponding labels', function () {
        expect(minerva.conditions.conditionsTable.title).toEqual(testData.title)
        minerva.conditions.btnAll.click();
        expect(columns['Name'].data()).toEqual(testData.name);
        expect(columns['Status'].data()).toEqual(testData.status);
        expect(columns['Severity'].data()).toEqual(testData.severity);
        expect(columns['Period'].data()).toEqual(testData.period);
        expect(columns['Location'].data()).toEqual(testData.location);
        expect(columns['ConditionCode'].data()).toEqual(testData.conditionCode);
        expect(columns['ConditionDescription'].data()).toEqual(testData.conditionDescription);
    });

    afterEach(function () {
        minerva.logout();
    });
});
