/**
 * Created by sujata on 10/27/2016.
 */

var minerva = require('../../pages/minerva'),
    testData = require(minerva.getTestDataFilePath());
var _ = require('lodash');

describe('conditions table', function() {
    var columns;
    beforeAll(function () {
        minerva.login(browser.params.userInternal);
        minerva.patients.txtName.sendKeys(testData.patientName);
        minerva.patients.btnSearch.click();
        minerva.patients.patientsTable.row(1).then(function (rowOne) {
            rowOne.patientDetails().click();
        });
        minerva.navbar.innerTab('Conditions').then(function (conditionsTab) {
            conditionsTab.visit();
        });
        columns = minerva.conditions.conditionsTable.columns;
    });

    it('MI-2573 if any field is missing unavailable should be visible', function () {
        expect(minerva.conditions.conditionsTable.title).toEqual(testData.title)
        minerva.conditions.btnAll.click();
        expect(columns['Location'].data()).toEqual(testData.location);
    });

    afterEach(function () {
        minerva.logout();
    });
});
