/**
 * Created by sujata on 10/25/2016.
 */
var minerva = require('../../pages/minerva'),
    testData = require(minerva.getTestDataFilePath());
var _ = require('lodash');

describe('allergies table', function() {
    var columns;
    beforeAll(function () {
        minerva.login(browser.params.userInternal);
        minerva.patients.txtName.sendKeys(testData.patientName);
        minerva.patients.btnSearch.click();
        minerva.patients.patientsTable.row(1).then(function (rowOne) {
            rowOne.patientDetails().click();
        });
        minerva.navbar.innerTab('Allergies').then(function (allergyTab) {
            allergyTab.visit();
        });
        minerva.allergy.allergyTable.columns.then(function (allColumns) {
            columns = allColumns;
        });
    });

    it('MI-2578 if any field is missing unavailable should be visible', function () {
        expect(minerva.allergy.allergyTable.title).toEqual(testData.title)
        expect(columns['Source'].data()).toEqual(testData.source);
    });

    afterEach(function () {
        minerva.logout();
    });
});
