/**
 * Created by sujata on 10/26/2016.
 */
var minerva = require('../../pages/minerva'),
    testData = require(minerva.getTestDataFilePath());
var _ = require('lodash');

describe('allergies table', function() {
    var columns;
    beforeAll(function () {
        minerva.login(browser.params.userInternal);
        minerva.patients.txtName.sendKeys(testData.patientName);
        minerva.patients.btnSearch.click();
        minerva.patients.patientsTable.row(1).then(function (rowOne) {
            rowOne.patientDetails().click();
        });
        minerva.navbar.innerTab('Allergies').then(function (allergyTab) {
            allergyTab.visit();
        });
    });

    it('MI-2580 if allergy submenu item is present patienr details should be there', function () {
        expect(minerva.allergy.allergyTable.title).toEqual('Allergies')
        expect(minerva.allergy.txtPatientName.getText()).toEqual(testData.patientName)
        expect(minerva.allergy.txtPatientId.getText()).toEqual(testData.valuePatId)
        expect(minerva.allergy.txtDob.getText()).toEqual(testData.valueDob)
        expect(minerva.allergy.lblBorn.getText()).toEqual(testData.lblBorn)
        expect(minerva.allergy.lblGender.getText()).toEqual(testData.lblGender)
        expect(minerva.allergy.lblPatientId.getText()).toEqual(testData.lblPatientId)
    });

    afterEach(function () {
        minerva.logout();
    });
});
