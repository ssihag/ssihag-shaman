/**
 * Created by sujata on 10/25/2016.
 */
var minerva = require('../../pages/minerva'),
    testData = require(minerva.getTestDataFilePath());
var _ = require('lodash');

describe('allergies table', function() {
    var columns;
    beforeAll(function () {
        minerva.login(browser.params.userInternal);
        minerva.patients.txtName.sendKeys(testData.patientName);
        minerva.patients.btnSearch.click();
        minerva.patients.patientsTable.row(1).then(function (rowOne) {
            rowOne.patientDetails().click();
        });
        minerva.navbar.innerTab('Allergies').then(function (allergyTab) {
            allergyTab.visit();
        });

        minerva.allergy.allergyTable.columns.then(function (allColumns) {
            columns = allColumns;
        });

    });

    it('MI-2445 verify heading labels', function () {
        expect(_.keys(columns)).toEqual(testData.cols);
       // expect(_.keys(columns)).toEqual(testData.cols);
    });

    afterEach(function () {
        minerva.logout();
    });
});