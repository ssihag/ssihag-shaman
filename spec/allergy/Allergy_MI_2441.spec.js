var minerva = require('../../pages/minerva'),
    testData = require(minerva.getTestDataFilePath());


describe('allergies table', function() {
    beforeAll(function () {
        minerva.login(browser.params.userInternal);
        minerva.patients.txtName.sendKeys(testData.patientName);
        minerva.patients.btnSearch.click();
        minerva.patients.patientsTable.row(1).then(function (rowOne) {
            rowOne.patientDetails().click();
        });
    });

    it('MI-2441 click on allergy submenu item', function () {
        minerva.navbar.innerTab('Allergies').then(function (allergyTab) {
            allergyTab.visit();
        });
        expect(minerva.allergy.allergyTable.title).toEqual(testData.title)
    });

    afterEach(function () {
        minerva.logout();
    });
});