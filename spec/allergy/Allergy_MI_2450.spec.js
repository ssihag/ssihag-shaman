/**
 * Created by sujata on 10/25/2016.
 */
/**
 * Created by sujata on 10/25/2016.
 */

/**
 * Created by sujata on 10/24/2016.
 */
var minerva = require('../../pages/minerva'),
    testData = require(minerva.getTestDataFilePath());
var _ = require('lodash');

describe('allergies table', function() {
    var columns;
    beforeAll(function () {
        minerva.login(browser.params.userInternal);
        minerva.patients.txtName.sendKeys(testData.patientName);
        minerva.patients.btnSearch.click();
        minerva.patients.patientsTable.row(1).then(function (rowOne) {
            rowOne.patientDetails().click();
        });
        minerva.navbar.innerTab('Allergies').then(function (allergyTab) {
            allergyTab.visit();
        });
    });

    it('MI-2450 verify spinner till loading of page', function () {
        expect(minerva.allergy.spinner.isDisplayed()).toBeTruthy();
        expect(minerva.allergy.allergyTable.title).toEqual('Allergies');
    });

    afterEach(function () {
        minerva.logout();
    });
});

