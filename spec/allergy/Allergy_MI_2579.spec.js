/**
 * Created by sujata on 10/26/2016.
 */

var minerva = require('../../pages/minerva'),
    testData = require(minerva.getTestDataFilePath());
var _ = require('lodash');

describe('allergies table', function() {
    var columns;
    beforeAll(function () {
        minerva.login(browser.params.userInternal);
        minerva.patients.txtName.sendKeys(testData.patientName);
        minerva.patients.btnSearch.click();
        minerva.patients.patientsTable.row(1).then(function (rowOne) {
            rowOne.patientDetails().click();
        });
        minerva.navbar.innerTab('Allergies').then(function (allergyTab) {
            expect(allergyTab.isActive()).toBeFalsy();
        });
        minerva.navbar.innerTab('Allergies').then(function (allergyTab) {
            allergyTab.visit();
        });

    });

    it('MI-2579 change in color on click on allergy', function () {
        minerva.navbar.innerTab('Allergies').then(function (allergyTab) {
           expect(allergyTab.isActive()).toBeTruthy();
        });
    });

    afterEach(function () {
        minerva.logout();
    });
});

