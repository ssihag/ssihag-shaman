/**
 * Created by sujata on 10/25/2016.
 */

var minerva = require('../../pages/minerva'),
    testData = require(minerva.getTestDataFilePath());
var _ = require('lodash');

describe('allergies table', function() {
    var columns;
    beforeAll(function () {
        minerva.login(browser.params.userInternal);
        minerva.patients.txtName.sendKeys(testData.patientName);
        minerva.patients.btnSearch.click();
        minerva.patients.patientsTable.row(1).then(function (rowOne) {
            rowOne.patientDetails().click();
        });
        minerva.navbar.innerTab('Allergies').then(function (allergyTab) {
            allergyTab.visit();
        });
        minerva.allergy.allergyTable.columns.then(function (allColumns) {
            columns = allColumns;
        });
    });

    it('MI-2447 verify records should be sorted', function () {
        expect(columns['Name'].data()).toEqual(testData.name);
        expect(columns['Reaction'].data()).toEqual(testData.reaction);
        expect(columns['Severity'].data()).toEqual(testData.severity);
        expect(columns['Source'].data()).toEqual(testData.source);
    });

    afterEach(function () {
        minerva.logout();
    });
});
