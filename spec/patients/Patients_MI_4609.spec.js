/**
 * Created by aayushi on 10/18/2016.
 */

var minerva =require('../../pages/minerva')

describe('patients table', function() {

    beforeEach(function () {
        minerva.login(browser.params.userInternal);
        columns = minerva.patients.patientsTable.columns;
    });

    it('MI-4609 enter only age to field and then search', function () {
        expect(minerva.patients.ToDateInputBox.isEnabled()).toBeFalsy();
    });

    afterEach(function () {
        minerva.logout();
    });
});