/**
 * Created by aayushi on 10/14/2016.
 */

var testData=require('./Patients_MI_4530.json'),
    minerva =require('../../pages/minerva');

describe('patients table', function() {

    beforeEach(function () {
        minerva.login(browser.params.userInternal);
        columns = minerva.patients.patientsTable.columns;
    });

    it('MI-4530 enter only spaces in email field and then search', function () {
        minerva.patients.txtEmail.sendKeys(testData.searchString1);
        minerva.patients.btnSearch.click();
        expect(columns['patientDetails'].data()).toEqual(testData.patientDetails);
    });

    afterEach(function () {
        minerva.logout();
    });
});