/**
 * Created by aayushi on 10/14/2016.
 */

var testData=require('./Patients_MI_4531.json'),
    minerva =require('../../pages/minerva');

describe('patients table', function() {

    beforeEach(function () {
        minerva.login(browser.params.userInternal);
        columns = minerva.patients.patientsTable.columns;
    });

    it('MI-4531 search by entering any invalid email', function () {
        minerva.patients.txtEmail.sendKeys(testData.searchString1);
        minerva.patients.btnSearch.click();
        expect(minerva.patients.msgNoResults.getText()).toEqual(testData.searchResult);
    });

    afterEach(function () {
        minerva.logout();
    });
});