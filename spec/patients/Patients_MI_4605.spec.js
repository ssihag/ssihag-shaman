/**
 * Created by aayushi on 10/18/2016.
 */

var testData=require('./Patients_MI_4605.json'),
    minerva =require('../../pages/minerva');

describe('patients table', function() {

    beforeEach(function () {
        minerva.login(browser.params.userInternal);
        columns = minerva.patients.patientsTable.columns;
    });
    it('MI-4605 enter zero in both age fields and then search', function () {
        minerva.patients.fromDateInputBox.sendKeys(testData.searchString1);
        minerva.patients.ToDateInputBox.sendKeys(testData.searchString2);
        minerva.patients.btnSearch.click();
        expect(minerva.patients.msgNoResults.getText()).toEqual(testData.alert);
    });

    afterEach(function () {
        minerva.logout();
    });
});