/**
 * Created by aayushi on 10/17/2016.
 */

var testData=require('./Patients_MI_4604.json'),
    minerva =require('../../pages/minerva');

describe('patients table', function() {

    beforeEach(function () {
        minerva.login(browser.params.userInternal);
        columns = minerva.patients.patientsTable.columns;
    });
    it('MI-4604 enter alphanumeric values in age field and then search', function () {
        minerva.patients.fromDateInputBox.sendKeys(testData.searchString);
        minerva.patients.lblAge.click();
        expect(minerva.patients.ageAlert.getText()).toEqual(testData.alert);
    });

    afterEach(function () {
        minerva.logout();
    });
});