/**
 * Created by aayushi on 10/13/2016.
 */

var testData=require('./Patients_MI_4526.json'),
    minerva =require('../../pages/minerva');

describe('patients table', function() {

    beforeEach(function () {
        minerva.login(browser.params.userInternal);
        columns = minerva.patients.patientsTable.columns;
    });

    it('MI-4526 search by entering any invalid Pat Id', function () {
        minerva.patients.txtPatientId.sendKeys(testData.searchString1);
        minerva.patients.btnSearch.click();
        expect(minerva.patients.msgNoResults.getText()).toEqual(testData.searchResult);
    });

    afterEach(function () {
        minerva.logout();
    });
});