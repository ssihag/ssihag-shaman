/**
 * Created by aayushi on 10/18/2016.
 */

var testData=require('./Patients_MI_4610.json'),
    minerva =require('../../pages/minerva');

describe('patients table', function() {

    beforeEach(function () {
        minerva.login(browser.params.userInternal);
        columns = minerva.patients.patientsTable.columns;
    });
    it('MI-4610 enter age to less than age from', function () {
        minerva.patients.fromDateInputBox.sendKeys(testData.searchString1);
        minerva.patients.ToDateInputBox.sendKeys(testData.searchString2);
        minerva.patients.lblAge.click();
        expect(minerva.patients.ageAlert.getText()).toEqual(testData.alert);
    });

    afterEach(function () {
        minerva.logout();
    });
});
