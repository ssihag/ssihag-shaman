/**
 * Created by aayushi on 10/6/2016.
 */

var testData=require('./Patients_MI_4520.json'),
    minerva =require('../../pages/minerva');

describe('patients table', function() {

    beforeEach(function () {
        minerva.login(browser.params.userInternal);
        columns = minerva.patients.patientsTable.columns;
    });
    it('MI-4520 search by patient first name and last name', function () {
        minerva.patients.btnClear.click();
        minerva.patients.txtName.sendKeys(testData.searchString);
        minerva.patients.btnSearch.click();
        expect(columns['patientDetails'].data()).toEqual(testData.patientDetails);
        expect(columns['phoneEmail'].data()).toEqual(testData.phoneEmail);
        expect(columns['Id'].data()).toEqual(testData.Id);
        expect(columns['Gender'].data()).toEqual(testData.gender);
        expect(columns['DOB'].data()).toEqual(testData.DOB);
        expect(columns['visitedDate'].data()).toEqual(testData.visitedDate);
    });

    afterEach(function () {
        minerva.logout();
    });
});