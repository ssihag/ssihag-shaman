/**
 * Created by aayushi on 10/13/2016.
 */

var testData=require('./Patients_MI_4523.json'),
    minerva =require('../../pages/minerva');

describe('patients table', function() {
    beforeEach(function () {
        minerva.login(browser.params.userInternal);
        var row;
        columns = minerva.patients.patientsTable.columns;
    });

    it('MI-4523 perform search by non existing data', function () {
        minerva.patients.btnClear.click();
        minerva.patients.txtName.sendKeys(testData.searchString);
        minerva.patients.btnSearch.click();
        expect(minerva.patients.msgNoResults.getText()).toEqual(testData.searchResult);

    });

    afterEach(function () {
        minerva.logout();
    });
});
