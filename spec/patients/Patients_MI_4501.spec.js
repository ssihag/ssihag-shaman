/**
 * Created by aayushi on 9/30/2016.
 */

var _ = require('lodash'),
    testData=require('./Patients_MI_4501.json'),
    minerva=require('../../pages/minerva');

describe('patients table', function() {

    beforeEach(function () {
        minerva.login(browser.params.userInternal);
        columns = minerva.patients.patientsTable.columns;
    });

    it('MI-4501 patients tab UI', function () {
        expect(minerva.patients.patientsTable.title.getText()).toEqual(testData.tabTitle);
        expect(minerva.patients.lblSearchBox.getText()).toEqual(testData.searchPatients);
        expect(minerva.patients.txtName.isDisplayed).toBeTruthy();
        expect(minerva.patients.lblName.getText()).toEqual(testData.name);
        expect(minerva.patients.lblPatientId.getText()).toEqual(testData.patId);
        expect(minerva.patients.lblEmail.getText()).toEqual(testData.Email);
        expect(minerva.patients.lblPhone.getText()).toEqual(testData.Phone);
        expect(minerva.patients.lblName.getText()).toEqual(testData.Name);
        expect(minerva.patients.genderTitle.getText()).toEqual(testData.gender);
        expect(minerva.patients.lblAge.getText()).toEqual(testData.Age);
        expect(minerva.patients.DobTitle.getText()).toEqual(testData.DOB);
        expect(_.keys(columns)).toEqual(testData.columns);
    });

    afterEach(function () {
        minerva.logout();
    });
});