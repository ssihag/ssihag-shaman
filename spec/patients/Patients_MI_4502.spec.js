/**
 * Created by aayushi on 9/30/2016.
 */

var _       = require('lodash'),
    testData=require('./Patients_MI_4502.json'),
     minerva=require('../../pages/minerva');

describe('patients table', function() {

    beforeAll(function () {
        minerva.login(browser.params.userInternal);
        columns = minerva.patients.patientsTable.columns;
    });

    it('MI-4502 name', function () {
        minerva.patients.txtName.sendKeys(testData.searchString1);
        minerva.patients.btnSearch.click();
        expect(minerva.patients.msgNoResults.isDisplayed).toBeTruthy();
        expect(minerva.patients.msgNoResults.getText()).toEqual(testData.searchResult);
    });

    it('MI-4502 patID', function () {
        minerva.patients.txtName.clear();
        minerva.patients.txtPatientId.sendKeys(testData.searchString2);
        minerva.patients.btnSearch.click();
        expect(minerva.patients.msgNoResults.isDisplayed).toBeTruthy();
        expect(minerva.patients.msgNoResults.getText()).toEqual(testData.searchResult);
    });

    it('MI-4502 email', function () {
        minerva.patients.txtPatientId.clear();
        minerva.patients.txtEmail.sendKeys(testData.searchString3);
        minerva.patients.btnSearch.click();
        expect(minerva.patients.msgNoResults.isDisplayed).toBeTruthy();
        expect(minerva.patients.msgNoResults.getText()).toEqual(testData.searchResult);
    });

    it('MI-4502 phone', function () {
        minerva.patients.txtEmail.clear();
        minerva.patients.txtPhone.sendKeys(testData.searchString4);
        minerva.patients.btnSearch.click();
        expect(minerva.patients.msgNoResults.isDisplayed).toBeTruthy();
        expect(minerva.patients.msgNoResults.getText()).toEqual(testData.searchResult);
        minerva.patients.txtPhone.clear();
        minerva.patients.txtPhone.sendKeys(testData.searchString5);
        minerva.patients.btnSearch.click();
        expect(minerva.patients.msgNoResults.isDisplayed).toBeTruthy();
        expect(minerva.patients.msgNoResults.getText()).toEqual(testData.searchResult);
    });

    it('MI-4502 gender', function () {
        minerva.patients.GenderPleaseSelectTitle.click();
        expect(minerva.patients.genderFemaleText()).toEqual(testData.strFemale);
        expect(minerva.patients.genderMaleText()).toEqual(testData.strMale);
        expect(minerva.patients.genderUndifferentiatedText()).toEqual(testData.strUndifferentiated);
    });

    afterAll(function () {
        minerva.logout();
    });

});