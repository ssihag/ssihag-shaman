/**
 * Created by aayushi on 10/18/2016.
 */

var testData=require('./Patients_MI_4606.json'),
    minerva =require('../../pages/minerva');

describe('patients table', function() {

    beforeEach(function () {
        minerva.login(browser.params.userInternal);
        columns = minerva.patients.patientsTable.columns;
    });
    it('MI-4606 enter negative value in age field and then search', function () {
        minerva.patients.fromDateInputBox.sendKeys(testData.searchString);
        minerva.patients.lblAge.click();
        expect(minerva.patients.ageAlert.getText()).toEqual(testData.alert);
    });

    afterEach(function () {
        minerva.logout();
    });
});