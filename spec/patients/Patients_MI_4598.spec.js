/**
 * Created by aayushi on 10/14/2016.
 */

var testData=require('./Patients_MI_4598.json'),
    minerva =require('../../pages/minerva');

describe('patients table', function() {

    beforeEach(function () {
        minerva.login(browser.params.userInternal);
        columns = minerva.patients.patientsTable.columns;
    });

    it('MI-4598 enter only spaces in phone number field and then search', function () {
        minerva.patients.txtPhone.sendKeys(testData.searchString1);
        minerva.patients.btnSearch.click();
        expect(columns['patientDetails'].data()).toEqual(testData.patientDetails);
    });

    afterEach(function () {
        minerva.logout();
    });
});