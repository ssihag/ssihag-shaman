/**
 * Created by aayushi on 10/13/2016.
 */

var testData=require('./Patients_MI_4525.json'),
    minerva =require('../../pages/minerva');

describe('patients table', function() {

    beforeEach(function () {
        minerva.login(browser.params.userInternal);
        columns = minerva.patients.patientsTable.columns;
    });

    it('MI-4525 enter only spaces in Pat Id field and then search', function () {
        minerva.patients.txtPatientId.sendKeys(testData.searchString1);
        minerva.patients.btnSearch.click();
        expect(columns['patientDetails'].data()).toEqual(testData.patientDetails);
    });

    afterEach(function () {
        minerva.logout();
    });
});