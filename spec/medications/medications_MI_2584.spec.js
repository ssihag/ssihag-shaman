var minerva = require('../../pages/minerva'),
    testData = require(minerva.getTestDataFilePath());

describe('medications table', function() {
    beforeAll(function () {
        minerva.login(browser.params.userInternal);
        minerva.patients.txtName.sendKeys(testData.patientName);
        minerva.patients.btnSearch.click();
        minerva.patients.patientsTable.row(1).then(function (rowOne) {
            rowOne.patientDetails().click();
        });
    });

    it('MI-2584 verify medications submenu item', function () {
        minerva.navbar.innerTab('Medications').then(function (medicationTab) {
            expect(medicationTab.displayed()).toBeTruthy();
        });
    });

    afterEach(function () {
        minerva.logout();
    });
});