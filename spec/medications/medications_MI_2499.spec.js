/**
 * Created by sujata on 11/2/2016.
 */
var minerva = require('../../pages/minerva'),
    testData = require(minerva.getTestDataFilePath());


describe('medications table', function() {
    beforeAll(function () {
        minerva.login(browser.params.userInternal);
        minerva.patients.txtName.sendKeys(testData.patientName);
        minerva.patients.btnSearch.click();
        minerva.patients.patientsTable.row(1).then(function (rowOne) {
            rowOne.patientDetails().click();
        });
    });

    it('MI-2499 click on medications submenu item', function () {
        minerva.navbar.innerTab('Medications').then(function (medicationsTab) {
            medicationsTab.visit();
        });
        expect(minerva.medications.medicationsTable.title).toEqual(testData.title)
    });

    afterEach(function () {
        minerva.logout();
    });
});
