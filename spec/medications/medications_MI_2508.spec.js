/**
 * Created by sujata on 11/2/2016.
 */

var minerva = require('../../pages/minerva'),
    testData = require(minerva.getTestDataFilePath());
var _ = require('lodash');

describe('medications table', function() {
    var columns;
    beforeAll(function () {
        minerva.login(browser.params.userInternal);
        minerva.patients.txtName.sendKeys(testData.patientName);
        minerva.patients.btnSearch.click();
        minerva.patients.patientsTable.row(1).then(function (rowOne) {
            rowOne.patientDetails().click();
        });
        minerva.navbar.innerTab('Medications').then(function (medicationsTab) {
            medicationsTab.visit();
        });
    });

    it('MI-2508 verify spinner till loading of page', function () {
        expect(minerva.medications.spinner.isDisplayed()).toBeTruthy();
        expect(minerva.medications.medicationsTable.title).toEqual('Medications');
    });

    afterEach(function () {
        minerva.logout();
    });
});
