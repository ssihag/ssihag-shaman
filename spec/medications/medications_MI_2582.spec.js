/**
 * Created by sujata on 11/2/2016.
 */

var minerva = require('../../pages/minerva'),
    testData = require(minerva.getTestDataFilePath()),
    _ = require('lodash');

describe('medications table', function() {
    var columns;
    beforeAll(function () {
        minerva.login(browser.params.userInternal);
        minerva.patients.txtName.sendKeys(testData.patientName);
        minerva.patients.btnSearch.click();
        minerva.patients.patientsTable.row(1).then(function (rowOne) {
            rowOne.patientDetails().click();
        });
        minerva.navbar.innerTab('Medications').then(function (medicationTab) {
            expect(medicationTab.isActive()).toBeFalsy();
        });
        minerva.navbar.innerTab('Medications').then(function (medicationTab) {
            medicationTab.visit();
        });

    });

    it('MI-2582 change in color on click on Medications', function () {
        minerva.navbar.innerTab('Medications').then(function (allergyTab) {
            expect(allergyTab.isActive()).toBeTruthy();
        });
    });

    afterEach(function () {
        minerva.logout();
    });
});

