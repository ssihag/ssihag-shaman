/**
 * Created by sujata on 9/29/2016.
 */

var minerva = require('../../pages/minerva'),
    testData = require(minerva.getTestDataFilePath());

describe('results table', function() {
    beforeEach(function () {
        minerva.login(browser.params.userInternal);
        minerva.navbar.tab('Results').then(function (recordsTab) {
            recordsTab.visit();
            columns = minerva.results.resultsTable.columns;
        });
    });

    it('verification with valid dob: MI-3305', function () {
        minerva.results.btnDropdown.click();

        minerva.results.advanceSearch.monthDropdown.sendKeys('July');
        minerva.results.advanceSearch.Inputday.sendKeys('28');
        minerva.results.advanceSearch.Inputyear.sendKeys('1975')
        minerva.results.advanceSearch.patientTextAdvance.click();
        minerva.results.advanceSearch.btnAdvanceSearch.click()

        var count = parseInt(testData.dobSearchData[9]);
        expect(minerva.results.txtKeyword.getText()).toEqual('Patient DOB: 28 Jul 1975')
        expect(minerva.results.btnClear.isDisplayed()).toBeTruthy();
        expect(columns['PatientName'].data()).toEqual([testData.dobSearchData[0]]);
        expect(columns['ValueId'].data()).toEqual([testData.dobSearchData[1]]);
        expect(columns['ValueBorn'].data()).toEqual([testData.dobSearchData[2]]);
        expect(columns['ReportType'].data()).toEqual([testData.dobSearchData[4]]);
        expect(columns['ReportStatus'].data()).toEqual([testData.dobSearchData[5]]);
        expect(columns['ReportDescription'].data()).toEqual([testData.dobSearchData[6]]);
        expect(columns['ReportTime'].data()).toEqual([testData.dobSearchData[7]]);
        expect(columns['ReportLocation'].data()).toEqual([testData.dobSearchData[8]]);
        expect(columns['LabelId'].data()).toEqual(Array(count).fill(testData.IdText));
        expect(columns['LabelBorn'].data()).toEqual(Array(count).fill(testData.BornText));
        expect(minerva.results.getCurrentResultsRange()).toEqual('1 - '+count);
        expect(minerva.results.getTotalResultsCount()).toEqual(testData.dobSearchData[9]);
    });
    afterEach(function () {
        minerva.logout();
	});
});