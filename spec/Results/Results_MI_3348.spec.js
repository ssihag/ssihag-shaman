/**
 * Created by sujata on 10/13/2016.
 */
var minerva = require('../../pages/minerva'),
    testData = require(minerva.getTestDataFilePath());

describe('results table', function() {
    beforeEach(function () {
        minerva.login(browser.params.userInternal);
        minerva.navbar.tabResults.then(function (recordsTab) {
            recordsTab.visit();
            columns = minerva.results.resultsTable.columns;
        });
    });

    it('perform search for modality selected MI-3348', function () {
        minerva.results.btnDropdown.click();
        minerva.results.advanceSearch.modalityDropdown.sendKeys(testData.modality);
        minerva.results.advanceSearch.btnAdvanceSearch.click();
        var count = parseInt(testData.count);
        expect(columns['PatientName'].data()).toEqual(testData.patientNames);
        expect(columns['ValueId'].data()).toEqual(testData.patientIds);
        expect(columns['ValueBorn'].data()).toEqual(testData.patientdobs);
        expect(columns['ReportType'].data()).toEqual(testData.reportType);
        expect(columns['ReportStatus'].data()).toEqual(testData.reportStatus);
        expect(columns['ReportDescription'].data()).toEqual(testData.reportDescription);
        expect(columns['ReportTime'].data()).toEqual(testData.reportDate);
        expect(columns['ReportLocation'].data()).toEqual(testData.reportLocation);
        expect(columns['LabelId'].data()).toEqual(Array(testData.firstPageCount).fill(testData.idText));
        expect(columns['LabelBorn'].data()).toEqual(Array(testData.firstPageCount).fill(testData.bornText));
        expect(columns['ReportLabelIcon'].data()).toEqual(testData.reportLabelIcon);
        expect(columns['ImageAvailableIcon'].data()).toEqual(testData.imageLabelIcon);
        expect(minerva.results.txtKeyword.getText()).toEqual(testData.keywordModality)
        expect(minerva.results.btnClear.isDisplayed()).toBeTruthy();
        expect(minerva.results.getCurrentResultsRange()).toEqual('1 - '+count);
        expect(minerva.results.getTotalResultsCount()).toEqual(testData.count);

    });

    afterEach(function () {
        minerva.logout();
    });
});