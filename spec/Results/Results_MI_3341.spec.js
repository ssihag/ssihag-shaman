/**
 * Created by sujata on 10/7/2016.
 */
var minerva = require('../../pages/minerva'),
    testData = require(minerva.getTestDataFilePath());

describe('results table', function() {
    beforeEach(function () {
        minerva.login(browser.params.userInternal);
        minerva.navbar.tabResults.then(function (recordsTab) {
            recordsTab.visit();
            columns = minerva.results.resultsTable.columns;
        });
    });

    it('verify test status dropdown: MI-3341', function () {
        minerva.results.btnDropdown.click();
        minerva.results.advanceSearch.statusDropdown.click();
        expect(minerva.results.advanceSearch.statusPleaseSelect.getText()).toEqual(testData.PleaseSelectStatus)
        expect(minerva.results.advanceSearch.recordStatuslistText).toEqual(testData.testTypeList)

    });
    afterEach(function () {
        minerva.logout();
    });
});