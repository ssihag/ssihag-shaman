/**
 * Created by sujata on 9/28/2016.
 */

var minerva = require('../../pages/minerva'),
    testData = require(minerva.getTestDataFilePath());

describe('results table', function() {
    beforeEach(function () {
        minerva.login(browser.params.userInternal);
        minerva.navbar.tab('Results').then(function (recordsTab) {
            recordsTab.visit();
            columns = minerva.results.resultsTable.columns;
        });
    });

    it('search by non-existing patId ID: MI-3293', function () {
        minerva.results.btnDropdown.click();
        minerva.results.advanceSearch.InputPatientIdAdvance.sendKeys(testData.nonExistingData)
        minerva.results.advanceSearch.btnAdvanceSearch.click()
        expect(minerva.results.txtKeyword.getText()).toEqual('Patient ID: '+testData.nonExistingData)
        expect(minerva.results.btnClear.isDisplayed()).toBeTruthy();
        expect(minerva.results.noResultFound.getText()).toEqual(testData.noResultsText)
    });

    afterEach(function () {
        minerva.logout();
	});
});