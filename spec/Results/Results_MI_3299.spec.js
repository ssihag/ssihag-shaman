/**
 * Created by sujata on 9/28/2016.
 */

var minerva = require('../../pages/minerva'),
    testData = require(minerva.getTestDataFilePath());

describe('results table', function() {
    beforeEach(function () {
        minerva.login(browser.params.userInternal);
        minerva.navbar.tab('Results').then(function (recordsTab) {
            recordsTab.visit();
            columns = minerva.results.resultsTable.columns;
        });
    });

    it('selecting only month in dob: MI-3299', function () {
        minerva.results.btnDropdown.click();
        minerva.results.advanceSearch.monthDropdown.sendKeys(testData.month);
        minerva.results.advanceSearch.patientTextAdvance.click();
        expect(minerva.results.advanceSearch.errorText.getText()).toEqual(testData.notValidDate)
    });

    afterEach(function () {
        minerva.logout();
	});
});