/**
 * Created by sujata on 10/18/2016.
 */
var minerva = require('../../pages/minerva'),
    testData = require('./Results_MI_3383.json');

describe('results table', function() {
    beforeEach(function () {
        minerva.login(browser.params.userInternal);
        minerva.navbar.tabResults.then(function (recordsTab) {
            recordsTab.visit();
            columns = minerva.results.resultsTable.columns;
        });
    });

    it('search with data only in patient section ID: MI-3383', function () {
        minerva.results.btnDropdown.click();
        minerva.results.advanceSearch.InputPatientNameAdvance.sendKeys(testData.patientName);
        minerva.results.advanceSearch.InputPatientIdAdvance.sendKeys(testData.patientId);
        minerva.results.advanceSearch.Inputday.sendKeys(testData.date);
        minerva.results.advanceSearch.Inputyear.sendKeys(testData.year);
        minerva.results.advanceSearch.monthDropdown.sendKeys(testData.month);
        minerva.results.advanceSearch.btnAdvanceSearch.click()
        expect(columns['PatientName'].data()).toEqual(testData.patientNames);
        expect(columns['ValueId'].data()).toEqual(testData.PatientIds);
        expect(columns['ValueBorn'].data()).toEqual(testData.Patientdobs);
        expect(columns['ReportType'].data()).toEqual(testData.reportType);
        expect(columns['ReportStatus'].data()).toEqual(testData.reportStatus);
        expect(columns['ReportDescription'].data()).toEqual(testData.reportDescription);
        expect(columns['ReportTime'].data()).toEqual(testData.reportDate);
        expect(columns['ReportLocation'].data()).toEqual(testData.reportLocation);
        expect(columns['LabelId'].data()).toEqual(Array(testData.firstPageCount).fill(testData.IdText));
        expect(columns['LabelBorn'].data()).toEqual(Array(testData.firstPageCount).fill(testData.BornText));
        expect(columns['ReportLabelIcon'].data()).toEqual(testData.ReportLabelIcon);
        expect(columns['ImageAvailableIcon'].data()).toEqual(testData.ImageAvailableIcon);
        expect(minerva.results.btnClear.isDisplayed()).toBeTruthy();
        expect(minerva.results.getCurrentResultsRange()).toEqual('1 - '+testData.firstPageCount);
        expect(minerva.results.getTotalResultsCount()).toEqual(testData.totalRecords);
    });

    afterEach(function () {
        minerva.logout();
    });
});
