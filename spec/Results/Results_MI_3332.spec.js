/**
 * Created by sujata on 10/6/2016.
 */

var minerva = require('../../pages/minerva'),
    testData = require(minerva.getTestDataFilePath());

describe('results table', function() {
    beforeEach(function () {
        minerva.login(browser.params.userInternal);
        minerva.navbar.tab('Results').then(function (recordsTab) {
            recordsTab.visit();
            columns = minerva.results.resultsTable.columns;
        });
    });

    it('only spaces in order id field ID: MI-3332', function () {
        minerva.results.btnDropdown.click();

        minerva.results.advanceSearch.InputOrderIdAdvance.sendKeys('    ')
        minerva.results.advanceSearch.btnAdvanceSearch.click()

        expect(columns['PatientName'].data()).toEqual(testData.patientNames);
        expect(columns['ValueId'].data()).toEqual(testData.PatientIds);
        expect(columns['ValueBorn'].data()).toEqual(testData.Patientdobs);
        expect(columns['ReportType'].data()).toEqual(testData.reportType);
        expect(columns['ReportStatus'].data()).toEqual(testData.reportStatus);
        expect(columns['ReportDescription'].data()).toEqual(testData.reportDescription);
        expect(columns['ReportTime'].data()).toEqual(testData.reportDate);
        expect(columns['ReportLocation'].data()).toEqual(testData.reportLocation);
        expect(columns['LabelId'].data()).toEqual(Array(testData.firstPageCount).fill(testData.IdText));
        expect(columns['LabelBorn'].data()).toEqual(Array(testData.firstPageCount).fill(testData.BornText));
        expect(columns['ReportLabelIcon'].data()).toEqual(testData.ReportLabelIcon);
    });

    afterEach(function () {
        minerva.logout();
    });
});