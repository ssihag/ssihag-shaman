/**
 * Created by sujata on 9/28/2016.
 */

var minerva = require('../../pages/minerva'),
    testData = require(minerva.getTestDataFilePath());

describe('results table', function() {
    beforeEach(function () {
        minerva.login(browser.params.userInternal);
        minerva.navbar.tabResults.then(function (recordsTab) {
            recordsTab.visit();
        });
    });
    it('normal search ui ID: MI-3271', function () {
        console.log('verifying elements on records screen');
        expect(minerva.results.btnSearch.isDisplayed()).toBeTruthy();
        expect(minerva.results.txtSearch.isDisplayed()).toBeTruthy();
        expect(minerva.results.btnDropdown.isDisplayed()).toBeTruthy();
        expect(minerva.results.txtSearch.getAttribute('placeholder'), testData.searchPlaceholder).toBeTruthy();
        expect(minerva.results.btnSearch.isEnabled()).toBeTruthy();
    });

    afterEach(function () {
        minerva.logout();
	});
});