/**
 * Created by sujata on 9/29/2016.
 */

var minerva = require('../../pages/minerva'),
    testData = require(minerva.getTestDataFilePath());

describe('results table', function() {
    beforeEach(function () {
        minerva.login(browser.params.userInternal);
        minerva.navbar.tab('Results').then(function (recordsTab) {
            recordsTab.visit();
            columns = minerva.results.resultsTable.columns;
        });
    });

    it('non existing order name: MI-3328', function () {
        minerva.results.btnDropdown.click();

        minerva.results.advanceSearch.InputOrderNameAdvance.sendKeys(testData.nonExistingData)
        minerva.results.advanceSearch.btnAdvanceSearch.click()

        expect(minerva.results.txtKeyword.getText()).toEqual('Order Name: '+testData.nonExistingData)
        expect(minerva.results.btnClear.isDisplayed()).toBeTruthy();
        expect(minerva.results.noResultFound.getText()).toEqual(testData.noResultFound)
    });
    afterEach(function () {
        minerva.logout();
	});
});