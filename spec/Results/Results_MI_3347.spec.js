/**
 * Created by sujata on 10/13/2016.
 */
var minerva = require('../../pages/minerva'),
    testData = require(minerva.getTestDataFilePath());

describe('results table', function() {
    beforeEach(function () {
        minerva.login(browser.params.userInternal);
        minerva.navbar.tabResults.then(function (recordsTab) {
            recordsTab.visit();
            columns = minerva.results.resultsTable.columns;
        });
    });

    it('verify dropdown list of modality: MI-3347', function () {
        minerva.results.btnDropdown.click();
        minerva.results.advanceSearch.modalityDropdown.click();
        expect(minerva.results.advanceSearch.modalityPleaseSelect.getText()).toEqual(testData.pleaseSelect)
        expect(minerva.results.advanceSearch.modalityTypelistText).toEqual(testData.modalityList)
    });

    afterEach(function () {
        minerva.logout();
    });
});