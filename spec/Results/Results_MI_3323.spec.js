/**
 * Created by sujata on 9/29/2016.
 */

var minerva = require('../../pages/minerva'),

    testData = require(minerva.getTestDataFilePath());


describe('results table', function() {
    beforeEach(function () {
        minerva.login(browser.params.userInternal);
        minerva.navbar.tab('Results').then(function (recordsTab) {
            recordsTab.visit();
            columns = minerva.results.resultsTable.columns;
        });
    });

    it('verify gender dropdown: MI-3323', function () {
        minerva.results.btnDropdown.click();

        minerva.results.advanceSearch.Inputday.clear()
        minerva.results.advanceSearch.Inputyear.clear()
        minerva.results.advanceSearch.genderDropdown.click()
        expect(minerva.results.advanceSearch.genderlistText).toEqual(testData.genderList)

    });
    afterEach(function () {
        minerva.logout();
	});
});