/**
 * Created by sujata on 9/29/2016.
 */

var minerva = require('../../pages/minerva'),
    testData = require(minerva.getTestDataFilePath());

describe('results table', function() {
    beforeEach(function () {
        minerva.login(browser.params.userInternal);
        minerva.navbar.tab('Results').then(function (recordsTab) {
            recordsTab.visit();
            columns = minerva.results.resultsTable.columns;
        });
    });

    it('perform search for female: MI-3325', function () {
        minerva.results.btnDropdown.click();

        minerva.results.advanceSearch.genderDropdown.sendKeys('Female');
        minerva.results.advanceSearch.btnAdvanceSearch.click();

        expect(columns['PatientName'].data()).toEqual(testData.genderNameFemale);
        expect(columns['ValueId'].data()).toEqual(testData.genderIdFemale);
        expect(columns['ValueBorn'].data()).toEqual(testData.genderDobFemale);
        expect(columns['ReportType'].data()).toEqual(testData.genderReportTypeFemale);
        expect(columns['ReportStatus'].data()).toEqual(testData.genderReportStatusFemale);
        expect(columns['ReportDescription'].data()).toEqual(testData.genderReportDescriptionFemale);
        expect(columns['ReportTime'].data()).toEqual(testData.genderReportDateFemale);
        expect(columns['ReportLocation'].data()).toEqual(testData.ReportLocation);
        expect(columns['LabelId'].data()).toEqual(Array(testData.firstPageCount).fill(testData.IdText));
        expect(columns['LabelBorn'].data()).toEqual(Array(testData.firstPageCount).fill(testData.BornText));
        expect(columns['labelGender'].data()).toEqual(Array(testData.firstPageCount).fill("Gender"));
        expect(columns['ValueGender'].data()).toEqual(Array(testData.firstPageCount).fill("Female"));
        expect(columns['ReportLabelIcon'].data()).toEqual(testData.genderReportIconFemale);
        expect(columns['ImageAvailableIcon'].data()).toEqual(testData.genderImageIconFemale);
        expect(minerva.results.txtKeyword.getText()).toEqual('Gender: Female')
        expect(minerva.results.btnClear.isDisplayed()).toBeTruthy();
        expect(minerva.results.getCurrentResultsRange()).toEqual('1 - 10');
        expect(minerva.results.getTotalResultsCount()).toEqual(testData.genderCountFemale);
    });
    afterEach(function () {
        minerva.logout();
	});
});