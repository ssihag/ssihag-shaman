/**
 * Created by sujata on 10/7/2016.
 */

var minerva = require('../../pages/minerva'),
    testData = require(minerva.getTestDataFilePath());

describe('results table', function() {
    beforeEach(function () {
        minerva.login(browser.params.userInternal);
        minerva.navbar.tab('Results').then(function (recordsTab) {
            recordsTab.visit();
            columns = minerva.results.resultsTable.columns;
        });
    });

    it('valid order Id: MI-3335', function () {
        minerva.results.btnDropdown.click();

        minerva.results.advanceSearch.InputOrderIdAdvance.sendKeys(testData.OrderId)
        minerva.results.advanceSearch.btnAdvanceSearch.click()

        expect(minerva.results.txtKeyword.getText()).toEqual('Order ID: '+testData.OrderId)
        expect(minerva.results.btnClear.isDisplayed()).toBeTruthy();
        expect(columns['PatientName'].data()).toEqual(testData.orderIdPatientName);
        expect(columns['ValueId'].data()).toEqual(testData.orderIdPatientId);
        expect(columns['ValueBorn'].data()).toEqual(testData.orderIdDobs);
        expect(columns['ReportType'].data()).toEqual(testData.orderIdReportType);
        expect(columns['ReportStatus'].data()).toEqual(testData.orderIdReportStatus);
        expect(columns['ReportDescription'].data()).toEqual(testData.orderIdReportDescription);
        expect(columns['ReportTime'].data()).toEqual(testData.orderIdReportDate);
        expect(columns['ReportLocation'].data()).toEqual([]);
        expect(columns['ImageAvailableIcon'].data()).toEqual(testData.ImageIconorderId);
        expect(columns['ReportLabelIcon'].data()).toEqual(testData.ReportIconorderId);
    });
    afterEach(function () {
        minerva.logout();
    });
});