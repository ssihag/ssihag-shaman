/**
 * Created by sujata on 10/13/2016.
 */
var minerva = require('../../pages/minerva'),
    testData = require('./Results_MI_3352.json');

describe('results table', function() {
    beforeEach(function () {
        minerva.login(browser.params.userInternal);
        minerva.navbar.tabResults.then(function (recordsTab) {
            recordsTab.visit();
            columns = minerva.results.resultsTable.columns;
        });
    });

    it('Enter ordering physician last name only ID: MI-3352', function () {
        minerva.results.btnDropdown.click();
        var firstName=testData.orderingPhysicianFirstName.split(' ');
        minerva.results.advanceSearch.InputOrderingPhysician.sendKeys(firstName[0])
        minerva.results.advanceSearch.btnAdvanceSearch.click()
        expect(columns['PatientName'].data()).toEqual(testData.patientNames);
        expect(columns['ValueId'].data()).toEqual(testData.PatientIds);
        expect(columns['ValueBorn'].data()).toEqual(testData.Patientdobs);
        expect(columns['ReportType'].data()).toEqual(testData.reportType);
        expect(columns['ReportStatus'].data()).toEqual(testData.reportStatus);
        expect(columns['ReportDescription'].data()).toEqual(testData.reportDescription);
        expect(columns['ReportTime'].data()).toEqual(testData.reportDate);
        expect(columns['ReportLocation'].data()).toEqual(testData.reportLocation);
        expect(columns['LabelId'].data()).toEqual(Array(testData.firstPageCount).fill(testData.IdText));
        expect(columns['LabelBorn'].data()).toEqual(Array(testData.firstPageCount).fill(testData.BornText));
        expect(columns['ReportLabelIcon'].data()).toEqual(testData.ReportLabelIcon);
        expect(minerva.results.txtKeyword.getText()).toEqual(testData.keyword+firstName[0])
        expect(minerva.results.btnClear.isDisplayed()).toBeTruthy();
        expect(minerva.results.getCurrentResultsRange()).toEqual('1 - '+testData.firstPageCount);
        expect(minerva.results.getTotalResultsCount()).toEqual(testData.totalRecords);
    });

    afterEach(function () {
        minerva.logout();
    });
});
