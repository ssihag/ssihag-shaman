/**
 * Created by sujata on 9/28/2016.
 */

var minerva = require('../../pages/minerva'),
    testData = require(minerva.getTestDataFilePath());

describe('results table', function() {
    beforeEach(function () {
        minerva.login(browser.params.userInternal);
        minerva.navbar.tab('Results').then(function (recordsTab) {
            recordsTab.visit();
            columns = minerva.results.resultsTable.columns;
        });
    });

    it('cross button action ID: MI-3276', function () {
        minerva.results.btnDropdown.click();
        minerva.results.btnCrossAdvanceSearch.click();
        //expect(browser.isElementPresent(minerva.results.advanceSearch.patientTextAdvance)).toBeFalsy();
        expect(minerva.results.advanceSearch.patientTextAdvance.isPresent()).toBeFalsy();
    });
    afterEach(function () {
        minerva.logout();
	});
});