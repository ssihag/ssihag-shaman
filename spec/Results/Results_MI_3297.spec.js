/**
 * Created by sujata on 9/28/2016.
 */

var minerva = require('../../pages/minerva'),
    testData = require(minerva.getTestDataFilePath());

describe('results table', function() {
    beforeEach(function () {
        minerva.login(browser.params.userInternal);
        minerva.navbar.tab('Results').then(function (recordsTab) {
            recordsTab.visit();
            columns = minerva.results.resultsTable.columns;
        });
    });

    it('only date/month in dob: MI-3297', function () {
        minerva.results.btnDropdown.click();
        minerva.results.advanceSearch.InputPatientIdAdvance.clear();
        minerva.results.advanceSearch.monthDropdown.sendKeys(testData.month);
        minerva.results.advanceSearch.Inputday.sendKeys(testData.day);
        minerva.results.advanceSearch.patientTextAdvance.click();
        expect(minerva.results.advanceSearch.errorText.getText()).toEqual(testData.notValidDate)
        //expect(minerva.results.btnIsDisabledFromElement(minerva.results.advanceSearch.btnAdvanceSearch)).toBeTruthy()
    });

    afterEach(function () {
        minerva.logout();
	});
});