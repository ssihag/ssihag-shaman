
/**
 * Created by sujata on 9/28/2016.
 */
var minerva = require('../../pages/minerva'),
    testData = require(minerva.getTestDataFilePath());

describe('results table', function() {
    beforeEach(function () {
        minerva.login(browser.params.userInternal);
        minerva.navbar.tab('Results').then(function (recordsTab) {
            recordsTab.visit();
            columns = minerva.results.resultsTable.columns;
        });
    });

    it('normal search by full name ID: MI-3402', function () {
       minerva.results.btnDropdown.click();

        minerva.results.advanceSearch.InputPatientNameAdvance.sendKeys(testData.fullNameSearchString);
        minerva.results.advanceSearch.btnAdvanceSearch.click();

        var count = parseInt(testData.firstNameSearchData[4]);
        expect(columns['PatientName'].data()).toEqual(Array(count).fill(testData.firstNameSearchData[0]));
        expect(columns['ValueId'].data()).toEqual(Array(count).fill(testData.firstNameSearchData[1]));
        expect(columns['ValueBorn'].data()).toEqual(Array(count).fill(testData.firstNameSearchData[2]));
        expect(columns['ReportType'].data()).toEqual(testData.firstNameSearchReportType);
        expect(columns['ReportStatus'].data()).toEqual(testData.firstNameSearchReportStatus);
        expect(columns['ReportDescription'].data()).toEqual(testData.firstNameSearchReportDescription);
        expect(columns['ReportTime'].data()).toEqual(testData.firstNameSearchReportDate);
        expect(columns['LabelId'].data()).toEqual(Array(count).fill(testData.IdText));
        expect(columns['LabelBorn'].data()).toEqual(Array(count).fill(testData.BornText));
        expect(columns['ImageAvailableIcon'].data()).toEqual(testData.ImageIconName);
        expect(columns['ReportLabelIcon'].data()).toEqual(testData.ReportIconName);
        expect(minerva.results.txtKeyword.getText(), 'Keyword:'+testData.fullNameSearchString).toBeTruthy();
        expect(minerva.results.btnClear.isDisplayed()).toBeTruthy();
        expect(minerva.results.getCurrentResultsRange()).toEqual('1 - '+count);
        expect(minerva.results.getTotalResultsCount()).toEqual(testData.firstNameSearchData[4]);
	});

    afterEach(function () {
        minerva.logout();
	});
});