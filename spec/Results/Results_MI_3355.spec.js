/**
 * Created by sujata on 10/18/2016.
 */
var minerva = require('../../pages/minerva'),
    testData = require('./Results_MI_3355.json');

describe('results table', function() {
    beforeEach(function () {
        minerva.login(browser.params.userInternal);
        minerva.navbar.tabResults.then(function (recordsTab) {
            recordsTab.visit();
            columns = minerva.results.resultsTable.columns;
        });
    });

    it('search by non-existing data in performing location ID: MI-3355', function () {
        minerva.results.btnDropdown.click();
        minerva.results.advanceSearch.advanceSearch.InputOrderingPhysician.sendKeys(testData.nonExistingData)
        minerva.results.advanceSearch.btnAdvanceSearch.click()
        expect(minerva.results.txtKeyword.getText()).toEqual(testData.keyword+testData.nonExistingData)
        expect(minerva.results.btnClear.isDisplayed()).toBeTruthy();
        expect(minerva.results.advanceSearch.noResultFound.getText()).toEqual(testData.noResultsText)
    });

    afterEach(function () {
        minerva.logout();
    });
});