/**
 * Created by sujata on 10/7/2016.
 */
var minerva = require('../../pages/minerva'),
    testData = require(minerva.getTestDataFilePath());

describe('results table', function() {
    beforeEach(function () {
        minerva.login(browser.params.userInternal);
        minerva.navbar.tabResults.then(function (recordsTab) {
            recordsTab.visit();
            columns = minerva.results.resultsTable.columns;
        });
    });

    it('verify test type dropdown: MI-3337', function () {
        minerva.results.btnDropdown.click();
        expect(minerva.results.advanceSearch.recordTypeDropdownAll.getText()).toEqual('All')
        minerva.results.advanceSearch.recordTypeDropdown.click();
        expect(minerva.results.advanceSearch.testTypelistText).toEqual(testData.testTypeList)

    });
    afterEach(function () {
        minerva.logout();
    });
});