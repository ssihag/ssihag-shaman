/**
 * Created by sujata on 10/12/2016.
 */
var minerva = require('../../pages/minerva'),
    testData = require(minerva.getTestDataFilePath());

describe('results table', function() {
    beforeEach(function () {
        minerva.login(browser.params.userInternal);
        minerva.navbar.tabResults.then(function (recordsTab) {
            recordsTab.visit();
            columns = minerva.results.resultsTable.columns;
        });
    });

    it('perform search for "Partial" option ID: MI-3343', function () {
        minerva.results.btnDropdown.click();
        minerva.results.advanceSearch.statusDropdown.click()
        minerva.results.advanceSearch.statusDropdown.sendKeys('Partial');
        minerva.results.advanceSearch.btnAdvanceSearch.click()
        expect(minerva.results.txtKeyword.getText()).toEqual('Status: Partial')
        expect(minerva.results.btnClear.isDisplayed()).toBeTruthy();
        expect(minerva.results.advanceSearch.noResultFound.getText()).toEqual(testData.noResultFound)
    });

    afterEach(function () {
        minerva.logout();
    });
});