
/**
 * Created by sujata on 9/28/2016.
 */
var minerva = require('../../pages/minerva'),
    testData = require(minerva.getTestDataFilePath());

describe('results table', function() {
    beforeEach(function () {
        minerva.login(browser.params.userInternal);
        minerva.navbar.tabResults.then(function (recordsTab) {
            recordsTab.visit();
            columns = minerva.results.resultsTable.columns;
        });
    });

    it('normal search by test name ID: MI-3277', function () {
        minerva.results.txtSearch.sendKeys(testData.testNameSearchString);
        minerva.results.btnSearch.click();
        var count = parseInt(testData.testNameCount);
        expect(columns['PatientName'].data()).toEqual(Array(count).fill(testData.testNameSearchData[0]));
        expect(columns['ValueId'].data()).toEqual(Array(count).fill(testData.testNameSearchData[1]));
        expect(columns['ValueBorn'].data()).toEqual(Array(count).fill(testData.testNameSearchData[2]));
        expect(columns['ReportType'].data()).toEqual(testData.testNameSearchReportType);
        expect(columns['ReportStatus'].data()).toEqual(testData.testNameSearchReportStatus);
        expect(columns['ReportDescription'].data()).toEqual(testData.testNameSearchReportDescription);
        expect(columns['ReportTime'].data()).toEqual(testData.testNameSearchReportDate);
        expect(columns['LabelId'].data()).toEqual(Array(count).fill(testData.IdText));
        expect(columns['LabelBorn'].data()).toEqual(Array(count).fill(testData.BornText));
        expect(minerva.results.txtKeyword.getText(), 'Keyword:'+testData.testNameSearchString).toBeTruthy();
        expect(minerva.results.btnClear.isDisplayed()).toBeTruthy();
        expect(minerva.results.getCurrentResultsRange()).toEqual('1 - '+count);
        expect(minerva.results.getTotalResultsCount()).toEqual(testData.testNameCount);
    });

    afterEach(function () {
        minerva.logout();
	});
});