
   var minerva = require('../../pages/minerva'),
    testData = require(minerva.getTestDataFilePath());

describe('results table', function() {
    beforeEach(function () {
        minerva.login(browser.params.userInternal);
        minerva.navbar.tab('Results').then(function (recordsTab) {
            recordsTab.visit();
            columns = minerva.results.resultsTable.columns;
        });
    });

    it('advance search UI ID: MI-3278', function () {
        minerva.results.btnDropdown.click();
        expect(minerva.results.advanceSearchHeader.getText(),testData.advanceSearchHeader).toBeTruthy();
        expect(minerva.results.advanceSearch.patientTextAdvance.getText()).toEqual(testData.patientTextAdvance);
        expect(minerva.results.advanceSearch.patientNameTextAdvance.getText()).toEqual(testData.patientNameTextAdvance);
        expect(minerva.results.advanceSearch.patientIdTextAdvance.getText()).toEqual(testData.patientIdTextAdvance)
        expect(minerva.results.advanceSearch.patientDobTextAdvance.getText()).toEqual(testData.patientDobTextAdvance);
        expect(minerva.results.advanceSearch.patientGenderTextAdvance.getText()).toEqual(testData.patientGenderTextAdvance);
        expect(minerva.results.advanceSearch.orderNameTextAdvance.getText()).toEqual(testData.orderNameTextAdvance)
        expect(minerva.results.advanceSearch.orderDateFromTextAdvance.getText()).toEqual(testData.orderDateFromTextAdvance)
        expect(minerva.results.advanceSearch.statusTextAdvance.getText()).toEqual(testData.statusTextAdvance);
        expect(minerva.results.advanceSearch.recordTypeTextAdvance.getText()).toEqual(testData.recordTypeTextAdvance);
        expect(minerva.results.advanceSearch.orderIdTextAdvance.getText()).toEqual(testData.orderIdTextAdvance)
        expect(minerva.results.advanceSearch.orderDateToTextAdvance.getText()).toEqual(testData.orderDateToTextAdvance)
        expect(minerva.results.advanceSearch.modalityTextAdvance.getText()).toEqual(testData.modalityTextAdvance);
        expect(minerva.results.advanceSearch.orderingPhysicianTextAdvance.getText()).toEqual(testData.orderingPhysicianTextAdvance);
        expect(minerva.results.advanceSearch.performingLocationTextAdvance.getText()).toEqual(testData.performingLocationTextAdvance);
        expect(minerva.results.advanceSearch.uploadedByTextAdvance.getText()).toEqual(testData.uploadedByTextAdvance);
    //    expect(minerva.results.advanceSearch.textBulkUpload.getText()).toEqual('Uploaded from bulk uploader');
        expect(minerva.results.btnCrossAdvanceSearch.isDisplayed()).toBeTruthy();
        expect(minerva.results.advanceSearch.InputPatientNameAdvance.isDisplayed()).toBeTruthy();
        expect(minerva.results.advanceSearch.InputPatientIdAdvance.isDisplayed()).toBeTruthy();
        expect(minerva.results.advanceSearch.InputOrderIdAdvance.isDisplayed()).toBeTruthy();
        expect(minerva.results.advanceSearch.InputOrderDateFrom.isDisplayed()).toBeTruthy();
        expect(minerva.results.advanceSearch.InputOrderNameAdvance.isDisplayed()).toBeTruthy();
        expect(minerva.results.advanceSearch.InputOrderDateTo.isDisplayed()).toBeTruthy();
        expect(minerva.results.advanceSearch.InputPerformingLoaction.isDisplayed()).toBeTruthy();
        expect(minerva.results.advanceSearch.InputOrderingPhysician.isDisplayed()).toBeTruthy();
        expect(minerva.results.advanceSearch.InputUploadedBy.isDisplayed()).toBeTruthy();
        expect(minerva.results.advanceSearch.Inputday.isDisplayed()).toBeTruthy();
        expect(minerva.results.advanceSearch.monthDropdown.isDisplayed()).toBeTruthy();
        expect(minerva.results.advanceSearch.Inputyear.isDisplayed()).toBeTruthy();
        expect(minerva.results.advanceSearch.modalityDropdown.isDisplayed()).toBeTruthy();
        expect(minerva.results.advanceSearch.genderDropdown.isDisplayed()).toBeTruthy();
        expect(minerva.results.advanceSearch.recordTypeDropdown.isDisplayed()).toBeTruthy();
        expect(minerva.results.advanceSearch.statusDropdown.isDisplayed()).toBeTruthy();

        expect(minerva.results.advanceSearch.btnClearAll.isDisplayed()).toBeTruthy();
        expect(minerva.results.advanceSearch.btnAdvanceSearch.isDisplayed()).toBeTruthy();
        expect(minerva.results.advanceSearch.checkboxBulkUpload.isDisplayed()).toBeTruthy();
//resultsf text identifier not  found
	});
    afterEach(function () {
        minerva.logout();
	});
});