   var minerva = require('../../pages/minerva'),
    testData = require(minerva.getTestDataFilePath());

describe('results table', function() {
    beforeEach(function () {
         minerva.login(browser.params.userInternal);
        minerva.navbar.tabResults.then(function (recordsTab) {
            recordsTab.visit();
        });
    });

    it('landing on records view ID: MI-2970', function () {
        expect(minerva.results.resultsTable.title).toEqual(testData.tabName);
    });

    afterEach(function () {
        minerva.logout();
    });
});