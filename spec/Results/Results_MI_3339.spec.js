/**
 * Created by sujata on 10/7/2016.
 */
var minerva = require('../../pages/minerva'),
    testData = require(minerva.getTestDataFilePath());

describe('results table', function() {
    beforeEach(function () {
        minerva.login(browser.params.userInternal);
        minerva.navbar.tabResults.then(function (recordsTab) {
            recordsTab.visit();
            columns = minerva.results.resultsTable.columns;
        });
    });

    it('perform search for "Radiology" option ID: MI-3339', function () {
        minerva.results.btnDropdown.click();
        minerva.results.advanceSearch.recordTypeDropdown.click()
        minerva.results.advanceSearch.recordTypeDropdown.sendKeys('Radiology')
        minerva.results.advanceSearch.btnAdvanceSearch.click()
        expect(columns['PatientName'].data()).toEqual(testData.patientNames);
        expect(columns['ValueId'].data()).toEqual(testData.PatientIds);
        expect(columns['ValueBorn'].data()).toEqual(testData.Patientdobs);
        expect(columns['ReportType'].data()).toEqual(testData.reportType);
        expect(columns['ReportStatus'].data()).toEqual(testData.reportStatus);
        expect(columns['ReportDescription'].data()).toEqual(testData.reportDescription);
        expect(columns['ReportTime'].data()).toEqual(testData.reportDate);
        expect(columns['ReportLocation'].data()).toEqual(testData.reportLocation);
        expect(columns['LabelId'].data()).toEqual(Array(testData.firstPageCount).fill(testData.IdText));
        expect(columns['LabelBorn'].data()).toEqual(Array(testData.firstPageCount).fill(testData.BornText));
        expect(columns['ReportLabelIcon'].data()).toEqual(testData.ReportLabelIcon);
        expect(columns['ImageAvailableIcon'].data()).toEqual(testData.ImageIconName);
        expect(minerva.results.txtKeyword.getText()).toEqual('Record Type: Radiology')
        expect(minerva.results.btnClear.isDisplayed()).toBeTruthy();
        expect(minerva.results.getCurrentResultsRange()).toEqual('1 - '+testData.firstPageCount);
        expect(minerva.results.getTotalResultsCount()).toEqual(testData.totalCount);
    });

    afterEach(function () {
        minerva.logout();
    });
});