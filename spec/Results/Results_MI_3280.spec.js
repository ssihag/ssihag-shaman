
var minerva = require('../../pages/minerva'),
    testData = require(minerva.getTestDataFilePath());

describe('results table', function() {
    beforeEach(function () {
        minerva.login(browser.params.userInternal);
        minerva.navbar.tab('Results').then(function (recordsTab) {
            recordsTab.visit();
            columns = minerva.results.resultsTable.columns;
        });
    });

    it('clear all button action ID: MI-3280', function () {
        minerva.results.btnDropdown.click();
        minerva.results.advanceSearch.InputPatientNameAdvance.sendKeys(testData.InputPatientNameAdvance)
        expect(minerva.results.advanceSearch.InputPatientNameAdvance.getAttribute('value')).toEqual(testData.InputPatientNameAdvance)
        minerva.results.btnClearAll.click();
        expect(minerva.results.advanceSearch.InputPatientNameAdvance.getAttribute('value')).toEqual('')
    });
    afterEach(function () {
        minerva.logout();
	});
});