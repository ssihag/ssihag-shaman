/**
 * Created by sujata on 9/29/2016.
 */

var minerva = require('../../pages/minerva'),
    testData = require(minerva.getTestDataFilePath());

describe('results table', function() {
    beforeEach(function () {
        minerva.login(browser.params.userInternal);
        minerva.navbar.tab('Results').then(function (recordsTab) {
            recordsTab.visit();
            columns = minerva.results.resultsTable.columns;
        });
    });

    it('date 29 in leap year in dob: MI-3302', function () {
        minerva.results.btnDropdown.click();

        minerva.results.advanceSearch.monthDropdown.sendKeys(testData.month);
        minerva.results.advanceSearch.Inputday.sendKeys(testData.day);
        minerva.results.advanceSearch.Inputyear.sendKeys(testData.year)
        minerva.results.advanceSearch.patientTextAdvance.click();
        expect(minerva.results.advanceSearch.errorText.getText()).toEqual(testData.notValidDate)

    });

    afterEach(function () {
        minerva.logout();
	});
});