/**
 * Created by sujata on 9/28/2016.
 */

var minerva = require('../../pages/minerva'),
    testData = require(minerva.getTestDataFilePath());

describe('results table', function() {
    beforeEach(function () {
        minerva.login(browser.params.userInternal);
        minerva.navbar.tab('Results').then(function (recordsTab) {
            recordsTab.visit();
            columns = minerva.results.resultsTable.columns;
        });
    });

    it('patient name leading trailing spaces ID: MI-3289', function () {
        minerva.results.btnDropdown.click();

        minerva.results.advanceSearch.InputPatientNameAdvance.sendKeys('   '+testData.firstNameSearchString+'    ')
        minerva.results.advanceSearch.btnAdvanceSearch.click()

        var count = parseInt(testData.firstNameSearchData[4]);
        console.log("total count of records ="+count);
        expect(columns['PatientName'].data()).toEqual(Array(count).fill(testData.firstNameSearchData[0]));
        expect(columns['ValueId'].data()).toEqual(Array(count).fill(testData.firstNameSearchData[1]));
        expect(columns['ValueBorn'].data()).toEqual(Array(count).fill(testData.firstNameSearchData[2]));
        expect(columns['ReportType'].data()).toEqual(testData.firstNameSearchReportType);
        expect(columns['ReportStatus'].data()).toEqual(testData.firstNameSearchReportStatus);
        expect(columns['ReportDescription'].data()).toEqual(testData.firstNameSearchReportDescription);
        expect(columns['ReportTime'].data()).toEqual(testData.firstNameSearchReportDate);
        expect(columns['LabelId'].data()).toEqual(Array(count).fill(testData.IdText));
        expect(columns['LabelBorn'].data()).toEqual(Array(count).fill(testData.BornText));
        expect(columns['ImageAvailableIcon'].data()).toEqual(testData.ImageIconName);
        expect(columns['ReportLabelIcon'].data()).toEqual(testData.ReportIconName);
        expect(minerva.results.txtKeyword.getText()).toEqual('Patient Name: '+testData.firstNameSearchString)
        expect(minerva.results.btnClear.isDisplayed()).toBeTruthy();
        expect(minerva.results.getCurrentResultsRange()).toEqual('1 - '+count);
        expect(minerva.results.getTotalResultsCount()).toEqual(testData.firstNameSearchData[4]);
    });

    afterEach(function () {
        minerva.logout();
	});
});