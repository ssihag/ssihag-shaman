/**
 * Created by Ashish Raina on 9/6/2016.
 *
 * refer: https://github.com/angular/protractor/blob/master/docs/webdriver-vs-protractor.md
 */

var Page = require('astrolabe').Page,
    resultsTable = require('./table');
    advanceSearch = require('./advanceSearch');

module.exports = Page.create({

    /**
     * return pre-defined Page Sub-Components
     * @return {Object} various Page UI Component Objects
     */
    url: {value: '/#/records'},
    resultsTable: {get: function () {return resultsTable}},
    advanceSearch: {
        get: function () {
            return advanceSearch;
        }
    },
    /**
     * Custom Assertion
     * @return {boolean}
     */
    isAllRowsCount: {
        value: function(expectedCount) {
            this.resultsTable.rows.then(function (rowsAll) {
                return rowsAll.length === expectedCount;
            });
        }},

    /**
     * Pagination Controls
     * @return {promise ElementFinder} Protractor elements
     */
    lblTotalResultsCount: { get: function () { return $('.pager_links span b:nth-child(2)');}},
    lblPageResultsRange:  { get: function () { return $('.pager_links span b:nth-child(1)');}},
    eleNextPage:     { get: function () { return $('.btn-group .fa-chevron-right');}},
    elePreviousPage: { get: function () { return $('.btn-group .fa-chevron-left')  // resolves promise, returns element!!
        // return  this.findElement(this.by.css('[class="fa fa-chevron-left"]'));    // returns a promise
    }},

    btnSearch: {get: function () {return $('.keywordSearchButton');}},
    txtSearch: {get: function () {return $('.input-group input');}},
    btnDropdown: {get: function () {return $('.input-group-addon.btn.btn-default.advanceSearchButton');}},
    advanceSearchContainer: {get: function () {return $('.advanceSearchContainer');}},
    advanceSearchHeader: {get: function () {return $('.searchHeader div');}},
    btnCrossAdvanceSearch: {get: function () {return $('.searchHeader span');}},
    txtKeyword: {get: function () {return $('.searchCriteria');}},
    btnClear: {get: function () {return $('.btn.btn-default.btn-sm.ng-binding');}},


    // Pagination Controls - Custom JS elements
    btnNextPage: {
        get: function () {
            return (this.btnFromElement(this.eleNextPage)); // Constructs btn with additional properties
        }
    },

    btnPreviousPage: {
        get: function () {
            return (this.btnFromElement(this.elePreviousPage)); // Constructs btn with additional properties
        }
    },

    btnFromElement: {
        value: function (btnElement) {
            var page = this; // this => resultsPage
            return {
                isDisabled: function () { return page.btnIsDisabledFromElement(btnElement); },  // this is NOT=> resultsPage inside method
                click: function () { btnElement.click(); },
            };
        }
    },

    btnIsDisabledFromElement: {
        value: function (btnElement) {
            return btnElement.getCssValue('color').then(function (fontColor) {
                var expectedFontColor = 'rgba(152, 152, 152, 1)'; // Grey font for inactive buttons
                return (fontColor === expectedFontColor);
            });
        }
    },


    getCurrentResultsRange: {value: function () {return this.lblPageResultsRange.getText(); }},
    getTotalResultsCount: { value: function () {return this.lblTotalResultsCount.getText(); }},

    isBtnPreviousPageDisabled: {
        get: function () {
            this.btnPreviousPage.getCssValue('color').then(function (fontColor) {
                var expectedFontColor = 'rgba(152, 152, 152, 1)'; // Grey font for inactive buttons
                return (fontColor === expectedFontColor);
            });
        }
    },

    btnIsEnabledFromElement: {
        value: function (btnElement) {
            return btnElement.getCssValue('color').then(function (fontColor) {
                var expectedFontColor = 'rgba(255, 255, 255, 1)';
                return (fontColor === expectedFontColor);
            });
        }
    },


    gotoNextPage: {
        value: function (loop) {
            loop = loop === undefined ? 1 : loop;
            while (loop) {
                this.eleNextPage.click();
                loop -= 1;
            }
        }
    },
    gotoPreviousPage: {
        value: function (loop) {
            loop = loop === undefined ? 1 : loop;
            while (loop) {
                this.elePreviousPage.click();
                loop -= 1;
            }
        }
    }
});