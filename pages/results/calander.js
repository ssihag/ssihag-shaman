/**
 * Created by sujata on 10/19/2016.
 */
var Page = require('astrolabe').Page;

module.exports = Page.create({
    tblCalander: {get: function () {return $$('.dropdown-menu.ng-valid.ng-valid-date-disabled').get(0);}},
    txtDate: {get: function () {return $$('.text-center.ng-scope .btn [class="ng-binding"]');}},
    txtWeek: {get: function () {return $$('[ng-repeat="label in labels track by $index"]');}},
    txtMonth: {get: function () {return $('.btn.btn-default.btn-sm strong');}},
    btnBackward:{get: function () {return $('.btn.btn-default.btn-sm.pull-left i');}},
    btnForward:{get: function () {return $('.btn.btn-default.btn-sm.pull-right i');}},
    btnToday:{get: function () {return $$('.btn.btn-sm.btn-info.ng-binding').get(0);}},
    btnClear:{get: function () {return $('.btn.btn-sm.btn-danger.ng-binding');}},
    btnClose:{get: function () {return $('.btn.btn-sm.btn-success.pull-right.ng-binding');}},
    lstMonths:{get: function () {return $$('[ng-repeat="dt in row track by dt.date"] .ng-binding');}},
    selectItem :      { value: function (val) { return element(by.cssContainingText('span', val)).click();}},

    lstMonthsText1: {
        get: function () {
            var list = [];
            return this.lstMonths.then(
                function (elements) { elements.forEach(
                    function (eachElement)
                    {
                        eachElement.isDisplayed().then(function (x) {
                            if(x)
                            {
                                eachElement.getText().then(
                                    function(str)
                                    {
                                        list.push(str)
                                    });
                            }
                        })
                    })
                    return list;
                } );
        }
    },

    txtMonthText : {
        get: function () {
            return this.txtMonth.getText().then(function (txt) {
                 return txt;
            })
        }
    }

});