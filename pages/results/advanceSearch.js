/**
 * Created by sujata on 10/17/2016.
 */
var Page = require('astrolabe').Page;

    module.exports = Page.create({
        patientTextAdvance: {get: function () {return $('.col-xs-12');}},
        patientNameTextAdvance: {get: function () {return $('[for=patientName]');}},
        InputPatientNameAdvance: {get: function () {return $('#patientName');}},
        patientIdTextAdvance: {get: function () {return $('[for=patientId]');}},
        InputPatientIdAdvance: {get: function () {return $('#patientId');}},
        patientDobTextAdvance: {get: function () {return $('[for=dob]');}},
        monthDropdown: {get: function () {return $('[data-ng-model=month]');}},
        Inputday: {get: function () {return $('[data-ng-model=day]');}},
        Inputyear: {get: function () {return $('[data-ng-model=year]');}},
        genderDropdown: {get: function () {return $('#gender');}},
        patientGenderTextAdvance: {get: function () {return $('[for=gender]');}},
        orderNameTextAdvance: {get: function () {return $('[for=orderName]');}},
        InputOrderNameAdvance: {get: function () {return $('#orderName');}},
        orderIdTextAdvance: {get: function () {return $('[for=orderId]');}},
        InputOrderIdAdvance: {get: function () {return $('#orderId');}},
        recordTypeTextAdvance: {get: function () {return $('[for=recordType]');}},
        recordTypeDropdown: {get: function () {return $('#recordType');}},
        recordTypeDropdownAll: {get: function () {return $('#recordType .ng-binding');}},
        statusTextAdvance: {get: function () {return $('[for=status]');}},
        statusDropdown: {get: function () {return $('#status');}},
        statusPleaseSelect: {get: function () {return $('#status .ng-binding');}},
        orderDateFromTextAdvance: {get: function () {return $('[for=orderDateFrom]');}},
        InputOrderDateFrom: {get: function () {return $('#orderDateFrom');}},
        orderDateToTextAdvance: {get: function () {return $('[for=orderDateTo]');}},
        InputOrderDateTo: {get: function () {return $('#orderDateTo');}},
        modalityTextAdvance: {get: function () {return $('[for=modality]');}},
        modalityDropdown: {get: function () {return $('#modality');}},
        modalityPleaseSelect: {get: function () {return $('#modality .ng-binding');}},
        orderingPhysicianTextAdvance: {get: function () {return $('[for=orderingPhysician]');}},
        InputOrderingPhysician: {get: function () {return $('#orderingPhysician');}},
        performingLocationTextAdvance: {get: function () {return $('[for=performingLocation]');}},
        InputPerformingLoaction: {get: function () {return $('#performingLocation');}},
        uploadedByTextAdvance: {get: function () {return $('[for=uploadedBy]');}},
        InputUploadedBy: {get: function () {return $('#uploadedBy');}},
        checkboxBulkUpload: {get: function () {return $('.checkCss');}},
        textBulkUpload: {get: function () {return $('.checkCss');}},
        btnAdvanceSearch: {get: function () {return  $$('.recordFilter.text-right.flip .btn-primary').get(0);}},
        btnClearAll: {get: function () {return $('.btn.btn-default.clearAllButton');}},
        noResultFound: {get: function () {return $('.mt15.noRecords div h4');}},
        errorText: {get: function () {return $('.errorText');}},
        monthDropdownList: {get: function () {return $$('[data-ng-bind=monthName]');}},
        genderDropdownList: {get: function () {return $$('[ng-repeat="option in serverConfig.availableGenderList"]');}},
        testTypeDropdownList: {get: function () {return $$('[id^=category]');}},
        testStatusDropdownList: {get: function () {return $$('[ng-repeat="option in serverConfig.availableRecordStatuses"]');}},
        modalityDropdownList: {get: function () {return $$('[ng-repeat="option in serverConfig.availableModalities"]');}},

        genderlistText:{
            get: function () {
                var list = [];
                return this.genderDropdownList.then(
                    function (elements) { elements.forEach(
                        function (eachElement) { eachElement.getText().then(
                            function (text) {
                                list.push(text.trim());
                            });
                        });
                        return list;
                    });
            }
        },

        recordStatuslistText:{
            get: function () {
                var list = [];
                return this.testStatusDropdownList.then(
                    function (elements) { elements.forEach(
                        function (eachElement) { eachElement.getText().then(
                            function (text) {
                                list.push(text.trim());
                            });
                        });
                        return list;
                    });
            }
        },


        testTypelistText:{
            get: function () {
                var list = [];
                return this.testTypeDropdownList.then(
                    function (elements) { elements.forEach(
                        function (eachElement) { eachElement.getText().then(
                            function (text) {
                                list.push(text.trim());
                            });
                        });
                        return list;
                    });
            }
        },
        modalityTypelistText:{
            get: function () {
                var list = [];
                return this.modalityDropdownList.then(
                    function (elements) { elements.forEach(
                        function (eachElement) { eachElement.getText().then(
                            function (text) {
                                list.push(text.trim());
                            });
                        });
                        return list;
                    });
            }
        },


        txtKeywordVisible:{
            get: function() {
                var ele = this.txtKeyword;
                return ele.isPresent().then(
                    function (result) {
                        if(result) {
                            return ele.isDisplayed();
                        }
                        else {
                            return false;
                        }
                    }
                );
            }
        },

        listText2:{
            get: function () {
                var list = [];
                return this.monthDropdownList.then(
                    function (elements) { elements.forEach(
                        function (eachElement) { eachElement.getText().then(
                            function (text) {
                                list.push(text);
                            });
                        });
                        return list;
                    }
                );
            }
        },





    });