/**
 * Created by sujata on 9/28/2016.
 */
var Page = require('astrolabe').Page;
var navbar = require('./navbar');
var login = require('./login');
var results = require('./results/results');
var patients = require('./patients/patients');

module.exports =Page.create({
    url:  { value: 'https://automation1.mphrx.com/webconnect' },

    navbar: {
        get: function () {
            return navbar;
        }
    },

    login: {
        get: function () {
            return login;
        }
    },

    // Base actions
    // Like `get:` was for page fields, `value:` signifies a function for the page object.
    login: {
        value: function (userObject) {
            this.go();
            browser.driver.manage().window().maximize();
            login.login(userObject);
            login.acceptAgreement();
        }
    },

    logout: {
        value: function () {
            navbar.logout();
            login.isLoggedOut()
        }
    },

    results: {
        get: function () {
            return results;
        }
    },

    patients: {
        get: function () {
            return patients;
        }
    }
});