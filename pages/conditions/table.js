/**
 * Created by sujata on 10/26/2016.
 */


var rows    = require('./table/rows'),
    columns = require('./table/columns'),
    Page    = require('astrolabe').Page;

module.exports = Page.create({

    tblAllergies: {
        get: function() {
            return $('#cndt_container');
        }
    },

    title: {
        get: function () {
            return $('.txt888.ng-binding').getText();
        }
    },

    rows: {
        get: function () {
            return rows.all;
        }
    },

    row: {
        value: function (rowNumber) {
            return rows.byNumber(rowNumber);
        }
    },

    columns: {
        get: function () {
            return columns.all;
        }
    },

    column: {
        value: function (columnName) {
            return columns.byName(columnName);
        }
    }
});