/**
 * Created by sujata on 10/26/2016.
 */
var Page = require('astrolabe').Page,
    conditionsTable = require('./table');

module.exports = Page.create({
    conditionsTable: {get: function () {return conditionsTable}},
    txtNoResults: {get: function () {return $('#cndt_noRecordText1 h4');}},
    spinner: {get: function () {return $('#spinnerContainer');}},
    lblHeading: {get: function () {return $$('#cndt_listH div div');}},
    lblBorn: {get: function () {return $('#patBnr_bornL');}},
    txtDob: {get: function () {return $('#patBnr_bornV');}},
    lblGender: {get: function () {return $('#patBnr_genderL');}},
    txtGender: {get: function () {return $('#patBnr_genderV1');}},
    lblPatientId: {get: function () {return $('#patBnr_IdL');}},
    txtPatientId: {get: function () {return $('#patBnr_IdV');}},
    txtPatientName: {get: function () {return $('#patBnr_title');}},
    btnActive: {get: function () {return $('#cndt_statusActive');}},
    btnAll: {get: function () {return $('#cndt_statusAll');}},

    lblHeadingText:{
        get: function () {
            var list = [];
            return this.lblHeading.then(
                function (elements) { elements.forEach(
                    function (eachElement) { eachElement.getText().then(
                        function (text) {
                            list.push(text.trim());
                        });
                    });
                    return list;
                });
        }
    },
})
