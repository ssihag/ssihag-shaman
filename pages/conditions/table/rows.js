/**
 * Created by sujata on 10/26/2016.
 */
/**
 * Created by sujata on 10/26/2016.

 */

var _    = require('lodash'),
    Page = require('astrolabe').Page;

module.exports = Page.create({

    tblRows: {
        get: function() {
            return $$('.table-row .row');
        }
    },

    length: {
        get: function () {
            return this.tblRows.length;
        }
    },

    all: {
        get: function () {
            var page = this;
            var rows = [];
            return this.tblRows.then(function (rowPromises) {
                _.forEach(rowPromises, function (rowPromise) {
                    //return rowPromise.then(function (row) {
                    rows.push(page.rowFromElement(rowPromise));
                    // });
                });
                return rows;
            });
        }
    },

    byNumber: {
        value: function (rowNumber) {
            // This functon is not zero-indexed (i.e., for the first row, use `getRowByNumber(1)`
            var page = this;
            return this.tblRows.then(function (dasboardsRows) {
                if (rowNumber < 1 || rowNumber > dasboardsRows.length) {
                    page.NoSuchRowException.thro('use row number 1 through ' + dasboardsRows.length);
                }
                return page.rowFromElement(dasboardsRows[rowNumber - 1]);
            });
        }
    },

    rowFromElement: {
        value: function (rowElement) {
            var page = this;
            return {
                name: function () { return page.rowNameFromElement(rowElement);},
                Status: function () { return page.rowStatusFromElement(rowElement);},
                severity: function () { return page.rowSeverityFromElement(rowElement);},
                Period: function () { return page.rowPeriodFromElement(rowElement);},
                Location: function () { return page.rowLocationFromElement(rowElement);},
                ConditionCode: function () { return page.rowConditionCodeFromElement(rowElement);},
                ConditionDescription: function () { return page.rowConditionDescriptionFromElement(rowElement);},
            };
        }
    },

    rowNameFromElement: {
        value: function (rowElement) {
            return rowElement.$('.black.dib.ng-binding').getText();
        }
    },

    rowStatusFromElement: {
        value: function (rowElement) {
            return rowElement.$('[id^=cndt_list_statusText_]').getText();
        }
    },

    rowSeverityFromElement: {
        value: function (rowElement) {
            return rowElement.$('[id^=cndt_list_severity_]').getText();
        }
    },

    rowPeriodFromElement: {
        value: function (rowElement) {
            return rowElement.$('[id^=cndt_list_period_]').getText();
        }
    },
    rowLocationFromElement: {
        value: function (rowElement) {
            return rowElement.$('[id^=cndt_list_locationF_]').getText();
        }
    } ,
    rowConditionCodeFromElement: {
        value: function (rowElement) {
            return rowElement.$('.dib.ng-binding.ng-scope').getText();
        }
    } ,
    rowConditionDescriptionFromElement: {
        value: function (rowElement) {
            return rowElement.$('[id^=cndt_list_nameDes_] em').getText();
        }
    }


});


