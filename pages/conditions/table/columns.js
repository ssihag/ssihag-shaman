/**
 * Created by sujata on 10/26/2016.
 */
/**
 * Created by sujata on 10/25/2016.
 */

var _ = require('lodash'),
    Page = require('astrolabe').Page,
    rows = require('./rows');

module.exports = Page.create({

    tblColumnHeadings: {
        get: function() { return $$('#cndt_listH div div'); }
    },

    all: {
        get: function () {
            var page = this;
            var columns = {};
            var tblColumnHeadings= ['Name','Status','Severity','Period','Location','ConditionCode','ConditionDescription']
            tblColumnHeadings.forEach(function(headingElement){
                columns[headingElement]=page.byName(headingElement)
            })
            return columns
        }
    },
    byName: {
        value: function (columnName) {
            var page = this;
            return {
                name: function () { return columnName; },
                data: function () { return page.columnDataFromName(columnName); }
            };
        }
    },

    columnDataFromName: {
        value: function (columnName) {
            switch (columnName) {
                case "Name":
                    return this.Name;
                case "Status":
                    return this.Status;
                case "Severity":
                    return this.Severity;
                case "Period":
                    return this.Period;
                case "Location":
                    return this.Location;
                case "ConditionCode":
                    return this.ConditionCode;
                case "ConditionDescription":
                    return this.ConditionDescription;
                default:
                    this.NoSuchColumnException.thro(columnHeading);
            }
        }
    },

    Name: {
        get: function () {
            var Name = [];
            return rows.tblRows.then(function (rowElements) {
                _.forEach(rowElements, function (rowElement) {
                    return rows.rowNameFromElement(rowElement).then(function (name) {
                        Name.push(name);
                    });
                });
                return Name;
            });
        }
    },


    Status: {
        get: function () {
            var Status = [];
            return rows.tblRows.then(function (rowElements) {
                _.forEach(rowElements, function (rowElement) {
                    return rows.rowStatusFromElement(rowElement).then(function (status) {
                        Status.push(status);
                    });
                });
                return Status;
            });
        }
    },

    Severity: {
        get: function () {
            var Severity = [];
            return rows.tblRows.then(function (rowElements) {
                _.forEach(rowElements, function (rowElement) {
                    return rows.rowSeverityFromElement(rowElement).then(function (severity) {
                        Severity.push(severity);
                    });
                });
                return Severity;
            });
        }
    },

    Period: {
        get: function () {
            var Period = [];
            return rows.tblRows.then(function (rowElements) {
                _.forEach(rowElements, function (rowElement) {
                    return rows.rowPeriodFromElement(rowElement).then(function (period) {
                        Period.push(period);
                    });
                });
                return Period;
            });
        }
    },

    Location: {
        get: function () {
            var Location = [];
            return rows.tblRows.then(function (rowElements) {
                _.forEach(rowElements, function (rowElement) {
                    return rows.rowLocationFromElement(rowElement).then(function (location) {
                        Location.push(location);
                    });
                });
                return Location;
            });
        }
    },
    ConditionCode: {
        get: function () {
            var ConditionCode = [];
            return rows.tblRows.then(function (rowElements) {
                _.forEach(rowElements, function (rowElement) {
                    return rows.rowConditionCodeFromElement(rowElement).then(function (conditionCode) {
                        ConditionCode.push(conditionCode);
                    });
                });
                return ConditionCode;
            });
        }
    },
    ConditionDescription: {
        get: function () {
            var ConditionDescription = [];
            return rows.tblRows.then(function (rowElements) {
                _.forEach(rowElements, function (rowElement) {
                    return rows.rowConditionDescriptionFromElement(rowElement).then(function (conditionDescription) {
                        ConditionDescription.push(conditionDescription);
                    });
                });
                return ConditionDescription;
            });
        }
    },

    NoSuchColumnException: {
        get: function() { return this.exception('No such column'); }
    }

});
