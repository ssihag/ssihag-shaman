/**
 * Created by sujata on 10/28/2016.
 */


var rows    = require('./table/rows'),
    columns = require('./table/columns'),
    Page    = require('astrolabe').Page;

module.exports = Page.create({

    tblMedications: {
        get: function() {
            return $('#mdcn_container');
        }
    },

    title: {
        get: function () {
            return $('.txt888.ng-binding').getText();
        }
    },

    rows: {
        get: function () {
            return rows.all;
        }
    },

    row: {
        value: function (rowNumber) {
            return rows.byNumber(rowNumber);
        }
    },

    columns: {
        get: function () {
            return columns.all;
        }
    },

    column: {
        value: function (columnName) {
            return columns.byName(columnName);
        }
    }
});