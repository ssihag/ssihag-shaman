/**
 * Created by sujata on 10/28/2016.
 */


var _ = require('lodash'),
    Page = require('astrolabe').Page,
    rows = require('./rows');

module.exports = Page.create({

    tblColumnHeadings: {
        get: function() { return $$('#mdcn_listH div div'); }
    },

    all: {
        get: function () {
            var page = this;
            var columns = {};
            // var c=[];
            return this.tblColumnHeadings.then(function (headingElements) {
                _.forEach(headingElements, function (headingElement) {
                    return headingElement.getText().then(function (headingText) {
                        columns[headingText] = page.byName(headingText);
                    });
                });
                return columns;
            });
        }
    },

    byName: {
        value: function (columnName) {
            var page = this;
            return {
                name: function () { return columnName; },
                data: function () { return page.columnDataFromName(columnName); }
            };
        }
    },

    columnDataFromName: {
        value: function (columnName) {
            switch (columnName) {
                case "Name":
                    return this.Name;
                case "Status":
                    return this.Status;
                case "Prescriber":
                    return this.Prescriber;
                case "Source":
                    return this.Source;
                default:
                    this.NoSuchColumnException.thro(columnHeading);
            }
        }
    },

    Name: {
        get: function () {
            var Name = [];
            return rows.tblRows.then(function (rowElements) {
                _.forEach(rowElements, function (rowElement) {
                    return rows.rowNameFromElement(rowElement).then(function (name) {
                        Name.push(name);
                    });
                });
                return Name;
            });
        }
    },


    Status: {
        get: function () {
            var Status = [];
            return rows.tblRows.then(function (rowElements) {
                _.forEach(rowElements, function (rowElement) {
                    return rows.rowstatusFromElement(rowElement).then(function (status) {
                        Status.push(status);
                    });
                });
                return Status;
            });
        }
    },

    Prescriber: {
        get: function () {
            var Prescriber = [];
            return rows.tblRows.then(function (rowElements) {
                _.forEach(rowElements, function (rowElement) {
                    return rows.rowPrescriberFromElement(rowElement).then(function (prescriber) {
                        Prescriber.push(prescriber);
                    });
                });
                return Prescriber;
            });
        }
    },

    Source: {
        get: function () {
            var Source = [];
            return rows.tblRows.then(function (rowElements) {
                _.forEach(rowElements, function (rowElement) {
                    return rows.rowSourceFromElement(rowElement).then(function (source) {
                        Source.push(source);
                    });
                });
                return Source;
            });
        }
    },

    NoSuchColumnException: {
        get: function() { return this.exception('No such column'); }
    }

});
