/**
 * Created by sujata on 10/28/2016.
 */


var _    = require('lodash'),
    Page = require('astrolabe').Page;

module.exports = Page.create({

    tblRows: {
        get: function() {
            return $$('.table-row .row');
        }
    },

    length: {
        get: function () {
            return this.tblRows.length;
        }
    },

    all: {
        get: function () {
            var page = this;
            var rows = [];
            return this.tblRows.then(function (rowPromises) {
                _.forEach(rowPromises, function (rowPromise) {
                    //return rowPromise.then(function (row) {
                    rows.push(page.rowFromElement(rowPromise));
                    // });
                });
                return rows;
            });
        }
    },

    byNumber: {
        value: function (rowNumber) {
            // This functon is not zero-indexed (i.e., for the first row, use `getRowByNumber(1)`
            var page = this;
            return this.tblRows.then(function (dasboardsRows) {
                if (rowNumber < 1 || rowNumber > dasboardsRows.length) {
                    page.NoSuchRowException.thro('use row number 1 through ' + dasboardsRows.length);
                }
                return page.rowFromElement(dasboardsRows[rowNumber - 1]);
            });
        }
    },

    rowFromElement: {
        value: function (rowElement) {
            var page = this;
            return {
                name: function () { return page.rowNameFromElement(rowElement);},
                status: function () { return page.rowstatusFromElement(rowElement);},
                prescriber: function () { return page.rowPrescriberFromElement(rowElement);},
                source: function () { return page.rowSourceFromElement(rowElement);},
            };
        }
    },

    rowNameFromElement: {
        value: function (rowElement) {
            return rowElement.$('.black.bold.fs_1_2.ng-binding').getText();
        }
    },

    rowstatusFromElement: {
        value: function (rowElement) {
            return rowElement.$('[id^=mdcn_list_statusV_]').getText();
        }
    },

    rowPrescriberFromElement: {
        value: function (rowElement) {
            return rowElement.$('[id^=mdcn_listV_doctor_]').getText();
        }
    },

    rowSourceFromElement: {
        value: function (rowElement) {
            return rowElement.$('[id^=algr_list_source] .ng-binding.ng-scope').getText();
        }
    },


});


