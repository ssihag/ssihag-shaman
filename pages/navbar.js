/**
 * Created by Ashish Raina on 8/28/2016.
 */
var _    = require('lodash'),
    Page = require('astrolabe').Page;

module.exports = Page.create({
    // `get:` is for page fields,
    // `value:` signifies a function for the page object.
    url:  { value: 'https://automation1.mphrx.com/webconnect' },

    /*
    * http://www.protractortest.org/#/locators
    * When using CSS Selectors as a locator, you can use the shortcut $() notation:
    *
    * element(by.css('some-css'));   =>     $('some-css');
    *
    * ElementFinder.prototype.$$
    * http://www.protractortest.org/#/api?view=ElementFinder.prototype.$$
    *
    * @param {string} a css selector
    * @returns {ElementArrayFinder}
    *
    * ElementArrayFinder extends Promise, and once an action is performed on
    * an ElementArrayFinder, the latest result can be accessed using then,
    * and will be returned as an array of the results.
    *
    */
    cssTabs: { // This is used by both `getTabByName` and `tblTabs`.
        get: function () {
            //return this.findElement(this.by.repeater('menu in menuItems'))  // returns a promise
            return $$('.collapse-item.ng-scope')  // resolves promise, returns element(s)!!
                .filter(function(listTabs) {
                    return listTabs.isDisplayed();});
        }
    },

    cssInnerTabs: { // This is used by both `getTabByName` and `tblTabs`.
        get: function () {
            return $$('li[id^=patBnr_link]')
                .filter(function(listTabs) {
                    return listTabs.isDisplayed();});
        }
    },

    tblTabs: {
        get: function () { return $$(this.cssTabs); }
    },

    /*
     Dynamic objects: Dynamic tabs object/function explained:-
     What "tabs()" does is:
        Iterate over all of the tabs we currently have
        Get some attribute X that uniquely identifies that tab ex: name
        Name the tab that attribute X  ex: tabs[name] //Tabs Object; property= name
        Assign the tab itself to that name
    */
    tabs: {
        get: function () {
            // First, we need to create a reference to `this`. Since we're using functions inside of other functions,
            // the scope of the `this` keyword changes. By copying the original scope, we can use it as we'd expect.
            var page = this;
            var tabs = {}; // new empty object
            return this.cssTabs.then(function (tabElements) {
                _.forEach(tabElements, function (tabElement) {
                    var tab = page.tabFromElement(tabElement);
                    return tab.name().then(function (name) {
                        tabs[name] = tab;
                    });
                });
                return tabs;
            });
        }
    },

    tab: {
        value: function (tabName) {
            var page = this;
            var tabHref = tabName.toLowerCase();
            tabHref = tabHref === 'system settings' ? 'sysSettings' : tabHref;
            tabHref = tabHref === 'results' ? 'records' : tabHref;
            var tabCss = '.collapse-item ' + '[href="#/' + tabHref  + '"].ng-binding';
            return $$(tabCss).then(function (tabs) {
                if (tabs.length) {
                    return page.tabFromElement(tabs[0]);
                } else {
                    page.NoSuchTabException.thro(tabName);
                }
            });
        }
    },

    // Instead of returning just a normal tab, we pass this result of tabFromElement,
    // a constructor builds us additional functionality.
    tabFromElement: {
        value: function (tabElement) {
            var page = this;
            return {
                name:       function () { return page.tabNameFromElement(tabElement); },
                isActive:   function () { return page.tabIsActiveFromElement(tabElement); },
                visit:      function () { tabElement.click(); }
            };
        }
    },

    tabIsActiveFromElement: {
        value: function (tabElement) {
            return tabElement.getText().then(function (name) {
            return tabElement.getCssValue('color').then(function (fontColor) {
            return tabElement.getCssValue('background-color').then(function (backgroundColor) {
                var expectedFontColor       = 'rgba(255, 255, 255, 1)'; // White font for active tabs
                var expectedBackGroundColor = 'rgba(35, 105, 166, 1)'; // Blue background for active tabs
                return (fontColor === expectedFontColor) && (backgroundColor === expectedBackGroundColor);
            });
            });
            });
        }
    },

    tabNameFromElement: {
        value: function (tabElement) {
            return tabElement.getText().then(function (name) {
                return name;
            });
        }
    },

    NoSuchTabException: {
        get: function () { return this.exception('No such tab'); }
    },


    // ***************************Inner Tabs *********************************
    innerTabs: {
        get: function () {
            var page = this;
            var innerTabs = {}; // new empty object
            return this.cssInnerTabs.then(function (innerTabsElements) {
                _.forEach(innerTabsElements, function (innerTabElement) {
                    var innerTab = page.innerTabFromElement(innerTabElement);
                    return innerTab.name().then(function (name) {
                        innerTabs[name] = innerTab;
                    });
                });
                return innerTabs;
            });
        }
    },

    innerTab: {
        value: function (innerTabName) {
            var page = this;
            var tabCss = '[title~="'+innerTabName+'"]';

            var tabHref = innerTabName.toLowerCase();
            switch (tabHref) {
                case "overview" :  var tabCss = '[href^="#/' + 'patientDetails'  + '"]'; break;
                case "family history" :  var tabCss = '[href$="' + 'familyMembers'  + '"]'; break;
                case "documents" :  var tabCss = '[title~="'+'Document'+'"]'; break;
                case "results" :  var tabCss =  '[title="Records"]'; break;
            }


            // We use `this.find.all` because it might return an empty list
            return $$(tabCss).then(function (innerTabs) {
                if (innerTabs.length) {
                    // If there is a list, we accept the first one as our tab
                    return page.innerTabFromElement(innerTabs[0]);
                } else {
                    // otherwise, the list is empty, and there is no such tab
                    page.NoSuchTabException.thro(innerTabName);
                }
            });
        }
    },

    innerTabFromElement: {
        value: function (tabElement) {
            var page = this;
            return {
                name:       function () { return page.tabNameFromElement(tabElement); },
                isActive:   function () { return page.innerTabIsActiveFromElement(tabElement); },
                visit:      function () { tabElement.click(); }
            };
        }
    },

    innerTabIsActiveFromElement: {
        value: function (tabElement) {
            return tabElement.getCssValue('color').then(function (fontColor) {
                return tabElement.getCssValue('background-color').then(function (backgroundColor) {
                    var expectedFontColor       = 'rgba(35, 105, 166, 1)'; // White font for active tabs
                    var expectedBackGroundColor = 'rgba(0, 0, 0, 0)'; // Blue background for active tabs
                    return (fontColor === expectedFontColor) && (backgroundColor === expectedBackGroundColor);
                });
            });
        }
    },

    lstUserProfile: { get: function () { return $$('.userName').get(0); }},
    btnLogout:      { get: function () { return $$('[ng-click="logout()"]'); }},
    lnkHome:        { get: function () { return $$('.clientLogo'); }},

    // actions
    logout: {
        value: function () {
            this.lstUserProfile.click();
            this.btnLogout.click();
        }
    },

    goHome: {
        value: function () {
            this.lnkHome.click();
        }
    },

    // assertions
    isLoggedIn: {
        value: function (username) {
            username = username === undefined ? this.driver.params.login.username : username;

            return this.lstUserProfile.getText().then(function (buttonText) {
             return buttonText === username;
             });
        }
    }
});