/**
 * Created by aayushi on 9/21/2016.
 */

var _ = require('lodash'),
    Page = require('astrolabe').Page;

module.exports = Page.create({

    tblRows: {
        get: function() { return $$('.table-row'); }
    },

    length: {
        get: function () { return this.tblRows.length; }
    },

    all: {
        get: function () {
            var page = this;
            var rows = [];
            return this.tblRows.then(function (rowPromises) {
                _.forEach(rowPromises, function (row) {
                    // return rowPromise.then(function (row) {
                    rows.push(page.rowFromElement(row));
                    // });
                });
                return rows;
            });
        }
    },

    byNumber: {
        value: function (rowNumber) {
            // This functon is not zero-indexed (i.e., for the first row, use `getRowByNumber(1)`
            var page = this;
            return this.tblRows.then(function (pollsRows) {
                if (rowNumber < 1 || rowNumber > pollsRows.length) {
                    page.NoSuchRowException.thro('use row number 1 through ' + pollsRows.length);
                }
                return page.rowFromElement(pollsRows[rowNumber - 1]);
            });
        }
    },

    rowFromElement: {
        value: function (rowElement) {
            var page = this;
            return {

                patientDetails: function () { return page.rowPatientNameDetailsFromElement(rowElement);},
                phoneEmail: function () { return page.rowPhoneEmailFromElement(rowElement);},
                visitedDate: function () { return page.rowLastVisitedDateFromElement(rowElement);},
                actionsOverview: function () { return page.rowActionsOverviewFromElement(rowElement);},
                actionsRadiology: function () { return page.rowActionsRadiologyFromElement(rowElement);},
                actionsLabs: function () { return page.rowActionsLabsFromElement(rowElement);},
                Id: function () { return page.rowIdFromElement(rowElement);},
                DOB: function () { return page.rowDOBFromElement(rowElement);},
                gender: function () { return page.rowGenderFromElement(rowElement);}

            };
        }
    },

    rowPatientNameDetailsFromElement: {
        value: function (rowElement) {
            return rowElement.$('.col-md-4 .fs_1_1').getText();
        }
    },

    rowPhoneEmailFromElement: {
        value: function (rowElement) {
            return rowElement.$('[id^="patL_list_phEmail_"]').getText();
        }
    },

    rowLastVisitedDateFromElement: {
        value: function (rowElement) {
            return rowElement.$('[id^=patL_list_lvDate_]').getText();
        }
    },

    rowActionsOverviewFromElement: {
        value: function (rowElement) {
            return rowElement.$('[id^=patL_list_aL1_]').getText();
        }
    },

    rowActionsLabsFromElement: {
        value: function (rowElement) {
            return rowElement.$('[id^=patL_list_aL2_]');
        }
    },

    rowActionsRadiologyFromElement: {
        value: function (rowElement) {
            return rowElement.$('[id^=patL_list_aL3_]');
        }
    },

    rowIdFromElement: {
        value: function (rowElement) {
            return rowElement.$('[id^=patL_list_patIdV]').getText();
        }
    },

    rowDOBFromElement: {
        value: function (rowElement) {
            return rowElement.$('[id^=patL_list_patBornV_]').getText();
        }
    },

    rowGenderFromElement: {
        value: function (rowElement) {
            return rowElement.$('[ng-if$="patient.gender"]').getText();
        }
    },

});