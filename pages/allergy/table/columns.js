/**
 * Created by sujata on 10/25/2016.
 */

var _ = require('lodash'),
    Page = require('astrolabe').Page,
    rows = require('./rows');

module.exports = Page.create({

    tblColumnHeadings: {
        get: function() { return $$('.table-header.hidden-xs.hidden-sm div .ng-binding'); }
    },

    all: {
        get: function () {
            var page = this;
            var columns = {};
           // var c=[];
            return this.tblColumnHeadings.then(function (headingElements) {
                _.forEach(headingElements, function (headingElement) {
                    return headingElement.getText().then(function (headingText) {
                        columns[headingText] = page.byName(headingText);
                    });
                });
                return columns;
            });
        }
    },

    byName: {
        value: function (columnName) {
            var page = this;
            return {
                name: function () { return columnName; },
                data: function () { return page.columnDataFromName(columnName); }
            };
        }
    },

    columnDataFromName: {
        value: function (columnName) {
            switch (columnName) {
                case "Name":
                    return this.Name;
                case "Reaction":
                    return this.Reaction;
                case "Severity":
                    return this.Severity;
                case "Source":
                    return this.Source;
                default:
                    this.NoSuchColumnException.thro(columnHeading);
            }
        }
    },

    Name: {
        get: function () {
            var Name = [];
            return rows.tblRows.then(function (rowElements) {
                _.forEach(rowElements, function (rowElement) {
                    return rows.rowNameFromElement(rowElement).then(function (name) {
                        Name.push(name);
                    });
                });
                return Name;
            });
        }
    },


    Reaction: {
        get: function () {
            var Reaction = [];
            return rows.tblRows.then(function (rowElements) {
                _.forEach(rowElements, function (rowElement) {
                    return rows.rowReactionFromElement(rowElement).then(function (reaction) {
                        Reaction.push(reaction);
                    });
                });
                return Reaction;
            });
        }
    },

    Severity: {
        get: function () {
            var Severity = [];
            return rows.tblRows.then(function (rowElements) {
                _.forEach(rowElements, function (rowElement) {
                    return rows.rowSeverityFromElement(rowElement).then(function (severity) {
                        Severity.push(severity);
                    });
                });
                return Severity;
            });
        }
    },

    Source: {
        get: function () {
            var Source = [];
            return rows.tblRows.then(function (rowElements) {
                _.forEach(rowElements, function (rowElement) {
                    return rows.rowSourceFromElement(rowElement).then(function (source) {
                        Source.push(source);
                    });
                });
                return Source;
            });
        }
    },

    NoSuchColumnException: {
        get: function() { return this.exception('No such column'); }
    }

});
