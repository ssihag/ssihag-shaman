/**
 * Created by sujata on 10/26/2016.

 */

var _    = require('lodash'),
    Page = require('astrolabe').Page;

module.exports = Page.create({

    tblRows: {
        get: function() {
            return $$('.table-row .row');
        }
    },

    length: {
        get: function () {
            return this.tblRows.length;
        }
    },

    all: {
        get: function () {
            var page = this;
            var rows = [];
            return this.tblRows.then(function (rowPromises) {
                _.forEach(rowPromises, function (rowPromise) {
                    //return rowPromise.then(function (row) {
                    rows.push(page.rowFromElement(rowPromise));
                    // });
                });
                return rows;
            });
        }
    },

    byNumber: {
        value: function (rowNumber) {
            // This functon is not zero-indexed (i.e., for the first row, use `getRowByNumber(1)`
            var page = this;
            return this.tblRows.then(function (dasboardsRows) {
                if (rowNumber < 1 || rowNumber > dasboardsRows.length) {
                    page.NoSuchRowException.thro('use row number 1 through ' + dasboardsRows.length);
                }
                return page.rowFromElement(dasboardsRows[rowNumber - 1]);
            });
        }
    },

    rowFromElement: {
        value: function (rowElement) {
            var page = this;
            return {
                name: function () { return page.rowNameFromElement(rowElement);},
                reaction: function () { return page.rowReactionFromElement(rowElement);},
                severity: function () { return page.rowSeverityFromElement(rowElement);},
                source: function () { return page.rowSourceFromElement(rowElement);},
            };
        }
    },

    rowNameFromElement: {
        value: function (rowElement) {
            return rowElement.$('.bold.black.ng-binding').getText();
        }
    },

    rowReactionFromElement: {
        value: function (rowElement) {
            return rowElement.$('.col-sm-6.col-md-4.xs_mt5.ng-binding').getText();
        }
    },

    rowSeverityFromElement: {
        value: function (rowElement) {
            return rowElement.$('[id^=algr_list_severity_]').getText();
        }
    },

    rowSourceFromElement: {
        value: function (rowElement) {
            return rowElement.$('[id^=algr_list_source] .ng-binding.ng-scope').getText();
        }
    },


});

