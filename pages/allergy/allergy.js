/**
 * Created by sujata on 10/24/2016.
 */
var Page = require('astrolabe').Page,
    allergyTable = require('./table');

module.exports = Page.create({
    allergyTable: {get: function () {return allergyTable}},
    txtNoResults: {get: function () {return $('#algr_noRecordsText1 .ng-binding');}},
    spinner: {get: function () {return $('#spinnerContainer');}},
    lblBorn: {get: function () {return $('#patBnr_bornL');}},
    txtDob: {get: function () {return $('#patBnr_bornV');}},
    lblGender: {get: function () {return $('#patBnr_genderL');}},
    txtGender: {get: function () {return $('#patBnr_genderV1');}},
    lblPatientId: {get: function () {return $('#patBnr_IdL');}},
    txtPatientId: {get: function () {return $('#patBnr_IdV');}},
    txtPatientName: {get: function () {return $('#patBnr_title');}},
})